package cz.mjanys.core;

import cz.mjanys.iface.Identifiable;

/**
 * @author Martin Janys
 */
public abstract class IdentifiableEntity implements Identifiable<Long> {

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IdentifiableEntity that = (IdentifiableEntity) o;

        if (getId() != null ? !getId().equals(that.getId()) : that.getId() != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return getId() != null ? getId().hashCode() : 0;
    }
}
