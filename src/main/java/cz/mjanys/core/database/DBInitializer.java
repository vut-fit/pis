package cz.mjanys.core.database;

import com.google.common.collect.Sets;
import cz.mjanys.core.entity.RoleEntity;
import cz.mjanys.core.entity.UserEntity;
import cz.mjanys.core.repository.RoleRepository;
import cz.mjanys.core.repository.UserRepository;
import cz.mjanys.core.util.PasswordEncoderUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;

/**
 * @author Martin Janys
 */
@Transactional
public class DBInitializer implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Value("${default-admin-login}")
    private String defaultAdminLogin;
    @Value("${default-admin-password}")
    private String defaultAdminPassword;
    @Value("${default-editor-login}")
    private String defaultEditorLogin;
    @Value("${default-editor-password}")
    private String defaultEditorPassword;
    @Value("${default-admin-role}")
    private String defaultAdminRole;
    @Value("${default-editor-role}")
    private String defaultEditorRole;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        initRoles();
        initUsers();
    }

    private void initRoles() {
        if (roleRepository.findByRole(defaultAdminRole) == null) {
            createRole(defaultAdminRole);
        }
        if (roleRepository.findByRole(defaultEditorRole) == null) {
            createRole(defaultEditorRole);
        }
    }

    private void createRole(String role) {
        RoleEntity roleEntity = new RoleEntity();
        roleEntity.setRole(role);
        roleRepository.save(roleEntity);
    }

    private void initUsers() {
        if (userRepository.count() == 0) {
            UserEntity userEntity = new UserEntity();
            userEntity.setUsername(defaultAdminLogin);
            userEntity.setPassword(PasswordEncoderUtil.encode(defaultAdminPassword));
            userEntity.setEnable(true);
            userEntity.setRoles(Sets.newHashSet(roleRepository.findAll()));
            userRepository.save(userEntity);

            userEntity = new UserEntity();
            userEntity.setUsername(defaultEditorLogin);
            userEntity.setPassword(PasswordEncoderUtil.encode(defaultEditorPassword));
            userEntity.setEnable(true);
            userEntity.setRoles(Sets.newHashSet(roleRepository.findByRole("EDITOR_ROLE")));
            userRepository.save(userEntity);
        }
    }
}
