package cz.mjanys.core.database;

import cz.mjanys.iface.Identifiable;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Martin Janys
 */
public class EntityReferenceFactory {

    @PersistenceContext
    private EntityManager entityManager;

    public <T> T getReference(Class<T> cls, Object pk) {
        return entityManager.getReference(cls, pk);
    }

    public <T> List<T> getReferences(Class<T> cls, Iterable<? extends Identifiable> list) {
        if (list != null) {
            List<T> references = new ArrayList<T>();
            for (Identifiable identifiable : list) {
                references.add(entityManager.getReference(cls, identifiable.getId()));
            }
            return references;
        }
        else {
            return Collections.emptyList();
        }
    }


}
