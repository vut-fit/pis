package cz.mjanys.core.entity;

import cz.mjanys.core.IdentifiableEntity;
import cz.mjanys.iface.DataLengthConstants;
import org.hibernate.annotations.*;
import org.hibernate.annotations.Cache;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author Martin Janys
 */
@Entity
@Table(name = "ARTICLES")
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ArticleEntity extends IdentifiableEntity {

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "TITLE", nullable = false, length = DataLengthConstants.STRING_SHORT)
    private String title;

    @Lob
    @Type(type = "org.hibernate.type.StringClobType")
    @Column(name = "CONTENT")
    private String content;

    @Column(name = "ACTION", length = DataLengthConstants.STRING_NORMAL)
    private String action;

    @Column(name = "ORDER_NUMBER")
    private Integer order;

    private String displayType;
    private String author;

    private Long pageId;
    private String articleType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDisplayType() {
        return displayType;
    }

    public void setDisplayType(String dislayType) {
        this.displayType = dislayType;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Long getPageId() {
        return pageId;
    }

    public void setPageId(Long pageId) {
        this.pageId = pageId;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public void setArticleType(String articleType) {
        this.articleType = articleType;
    }

    public String getArticleType() {
        return articleType;
    }
}
