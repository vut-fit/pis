package cz.mjanys.core.entity;

import cz.mjanys.core.IdentifiableEntity;
import cz.mjanys.iface.DataLengthConstants;
import org.hibernate.annotations.*;
import org.hibernate.annotations.Cache;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import java.util.List;

/**
 * @author Martin Janys
 */
@Entity
@Table(name = "PAGES")
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class PageEntity extends IdentifiableEntity {

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "TITLE", nullable = false, length = DataLengthConstants.STRING_SHORT)
    private String title;

    @Column(name = "FRIENDLY_URL", nullable = false, length = DataLengthConstants.STRING_SHORT, unique = true)
    private String friendlyUrl;

    @Column(name = "ORDER_NUMBER")
    private Integer order;

    private String layout;

    private String articleLayout;

    private boolean visible;

    @OneToMany(fetch = FetchType.EAGER, orphanRemoval = true, mappedBy = "pageId")
    @OrderBy("order")
    private List<ArticleEntity> articles;

    @ManyToOne(fetch = FetchType.LAZY)
    private PageEntity parent;

    @OneToMany(fetch = FetchType.LAZY, orphanRemoval = true, mappedBy = "parent")
    private List<PageEntity> childs;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFriendlyUrl() {
        return friendlyUrl;
    }

    public void setFriendlyUrl(String friendlyUrl) {
        this.friendlyUrl = friendlyUrl;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public List<ArticleEntity> getArticles() {
        return articles;
    }

    public void setArticles(List<ArticleEntity> articles) {
        this.articles = articles;
    }

    public String getLayout() {
        return layout;
    }

    public void setLayout(String layout) {
        this.layout = layout;
    }

    public String getArticleLayout() {
        return articleLayout;
    }

    public void setArticleLayout(String articleLayout) {
        this.articleLayout = articleLayout;
    }

    public PageEntity getParent() {
        return parent;
    }

    public void setParent(PageEntity parent) {
        this.parent = parent;
    }

    public List<PageEntity> getChilds() {
        return childs;
    }

    public void setChilds(List<PageEntity> childs) {
        this.childs = childs;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

}
