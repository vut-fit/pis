package cz.mjanys.core.entity;

import cz.mjanys.iface.DataLengthConstants;
import org.hibernate.annotations.*;
import org.hibernate.annotations.Cache;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Set;

/**
 * @author Martin Janys
 */
@Entity
@Table(name = "USERS")
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class UserEntity {

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "USERNAME", nullable = false, length=DataLengthConstants.STRING_SHORT, unique = true)
    private String username;

    @Column(name = "PASSWORD", nullable = false, length=DataLengthConstants.STRING_NORMAL)
    private String password;
    @Transient
    private boolean newPassword;

    @Column(name = "enable", nullable = false)
    private Boolean enable = true;

    @ManyToMany(fetch = FetchType.EAGER)
    private Set<RoleEntity> roles;

    @Transient
    public boolean isNew() {
        return (id == null);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        newPassword = true;
        this.password = password;
    }

    public Boolean isEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    public Boolean getEnable() {
        return enable;
    }

    public Set<RoleEntity> getRoles() {
        return roles;
    }

    public void setRoles(Set<RoleEntity> roles) {
        this.roles = roles;
    }

    public boolean isNewPassword() {
        return newPassword;
    }
}
