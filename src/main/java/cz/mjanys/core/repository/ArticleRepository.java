package cz.mjanys.core.repository;

import cz.mjanys.core.entity.ArticleEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Martin Janys
 */
@Repository
public interface ArticleRepository extends CrudRepository<ArticleEntity, Long> {
}
