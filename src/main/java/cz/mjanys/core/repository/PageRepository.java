package cz.mjanys.core.repository;

import cz.mjanys.core.entity.PageEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Martin Janys
 */
@Repository
public interface PageRepository extends CrudRepository<PageEntity, Long> {

    PageEntity findByFriendlyUrl(String friendlyUrl);

    List<PageEntity> findByParentOrderByOrderAsc(PageEntity parent);

    List<PageEntity> findByParentIdOrderByOrderAsc(Long id);

}
