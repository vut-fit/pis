package cz.mjanys.core.repository;

import cz.mjanys.core.entity.RoleEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Martin Janys
 */
@Repository
public interface RoleRepository extends CrudRepository<RoleEntity, Long> {

    RoleEntity findById(long id);

    RoleEntity findByRole(String roleName);

}
