package cz.mjanys.core.repository;

import cz.mjanys.core.entity.UserEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * @author Martin Janys
 */
@Repository
public interface UserRepository extends CrudRepository<UserEntity, Long> {

    UserEntity findById(long id);

    @Query("select u from UserEntity u where u.username = :username and u.enable = true")
    UserEntity findByUsernameEnabled(@Param("username") String username);

    UserEntity findByUsername(String username);

    UserEntity findByUsernameAndEnable(String username, boolean enable);

    long countByUsername(String username);
}
