package cz.mjanys.core.service;

import cz.mjanys.core.entity.ArticleEntity;
import cz.mjanys.core.repository.ArticleRepository;
import cz.mjanys.iface.converter.ArticleConverter;
import cz.mjanys.iface.dto.Article;
import cz.mjanys.iface.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

/**
 * @author Martin Janys
 */
@Transactional
@Service
public class ArticleServiceImpl implements ArticleService {

    @Autowired
    private ArticleRepository articleRepository;

    @Autowired
    private ArticleConverter articleConverter;

    @Transactional(readOnly = true)
    @Override
    public Article findArticleById(long id) {
        ArticleEntity articleEntity = articleRepository.findOne(id);
        Assert.notNull(articleEntity);
        return articleConverter.convert(articleEntity);
    }
}
