package cz.mjanys.core.service;

import cz.mjanys.core.entity.RoleEntity;
import cz.mjanys.core.entity.UserEntity;
import cz.mjanys.core.repository.UserRepository;
import cz.mjanys.iface.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.security.core.userdetails.User;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Martin Janys
 */
@Transactional(readOnly = true)
@Service("loginService")
public class LoginServiceImpl implements LoginService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserEntity user = userRepository.findByUsernameEnabled(username);
        if (user == null)
            throw new UsernameNotFoundException("Oops!");

        List<SimpleGrantedAuthority> authorities = authoritiesFor(user);

        return new User(username, user.getPassword(), authorities);
    }

    private List<SimpleGrantedAuthority> authoritiesFor(UserEntity user) {
        List<SimpleGrantedAuthority> authorities = new ArrayList<SimpleGrantedAuthority>();

        for (RoleEntity roleEntity : user.getRoles()){
            SimpleGrantedAuthority grantedAuthority = new SimpleGrantedAuthority(roleEntity.getRole());
            authorities.add(grantedAuthority);
        }

        return authorities;
    }
}
