package cz.mjanys.core.service;

import cz.mjanys.core.database.EntityReferenceFactory;
import cz.mjanys.core.entity.ArticleEntity;
import cz.mjanys.core.entity.PageEntity;
import cz.mjanys.core.repository.ArticleRepository;
import cz.mjanys.core.repository.PageRepository;
import cz.mjanys.iface.converter.ArticleEntityConverter;
import cz.mjanys.iface.converter.PageConverter;
import cz.mjanys.iface.converter.PageEntityConverter;
import cz.mjanys.iface.dto.Article;
import cz.mjanys.iface.dto.HirearchialPage;
import cz.mjanys.iface.dto.Page;
import cz.mjanys.iface.dto.RootPage;
import cz.mjanys.iface.service.PageService;
import cz.mjanys.web.page.PageConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Martin Janys
 */
@Transactional
@Service
public class PageServiceImpl implements PageService {

    @Autowired
    private EntityReferenceFactory referenceFactory;

    @Autowired
    private PageRepository pageRepository;

    @Autowired
    private ArticleRepository articleRepository;

    @Autowired
    private PageConverter pageConverter;
    @Autowired
    private PageEntityConverter pageEntityConverter;
    @Autowired
    private ArticleEntityConverter articleEntityConverter;

    @Value(PageConstants.PAGE_MAPPING)
    private String pageMapping;

    @Override
    public List<Page> findAllPages() {
        return pageConverter.convert(pageRepository.findAll());
    }

    @Override
    public long countPages() {
        return pageRepository.count();
    }

    @Override
    public RootPage getRootPage() {
        return new RootPage(pageConverter.convert(pageRepository.findByParentOrderByOrderAsc(null)));
    }

    @Transactional(readOnly = true)
    @Override
    public RootPage getPageTree() {
        List<PageEntity> rootPageEntities = pageRepository.findByParentOrderByOrderAsc(null);
        List<Page> rootPages = new ArrayList<Page>();
        for (PageEntity page : rootPageEntities) {
            rootPages.add(getPageSubTree(page));
        }

        return new RootPage(rootPages);
    }

    @Transactional(readOnly = true)
    @Override
    public Page getPageSubtreeTree(long id) {
        PageEntity page = pageRepository.findOne(id);
        Assert.notNull(page);
        return getPageSubTree(page);
    }

    private Page getPageSubTree(PageEntity pageEntity) {
        Page page = pageConverter.convert(pageEntity);
        List<PageEntity> childs = pageRepository.findByParentOrderByOrderAsc(pageEntity);
        List<Page> pages = new ArrayList<Page>();

        if (childs != null) {
            for (PageEntity child : childs) {
                Page childPage = getPageSubTree(child);
                pages.add(childPage);
            }
        }

        page.setChilds(pages);
        return page;
    }

    @Transactional(readOnly = true)
    @Override
    public Page findPageById(long id) {
        return pageConverter.convert(pageRepository.findOne(id));
    }

    @Override
    public HirearchialPage findPageByRequest(HttpServletRequest request) {
        String pathPrefix = request.getContextPath();
        String requestUri = request.getRequestURI().substring(pathPrefix.length());
        if (pathPrefix.equals("/"))
            pathPrefix = "";
        if (requestUri.startsWith(pageMapping)) { // page mapping
            pathPrefix += pageMapping + '/';
            String friendlyUrl = request.getRequestURI().substring(pathPrefix.length());
            return findPageByFriendlyUrl(friendlyUrl);
        }
        else if (requestUri.isEmpty() || requestUri.equals("/")){
            return getRootPage();
        }
        else {
            return null; // not page request
        }
    }

    @Override
    public Page savePage(Page page) {
        PageEntity entity = pageEntityConverter.convert(page);
        // todo
        entity = pageRepository.save(entity);
        return pageConverter.convert(entity);
    }

    @Override
    public void deletePageById(long id) {
        pageRepository.delete(id);
    }

    @Transactional(readOnly = true)
    @Override
    public Page findPageByFriendlyUrl(String friendlyUrl) {
        PageEntity page = pageRepository.findByFriendlyUrl(friendlyUrl);
        if (page != null) {
            return pageConverter.convert(page);
        }
        else {
            return null;
        }
    }

    @Override
    public void saveArticle(Article article) {
        ArticleEntity articleEntity = articleEntityConverter.convert(article);
        articleEntity = articleRepository.save(articleEntity);

        // add article to page
        PageEntity page = pageRepository.findOne(article.getPageId());
        List<ArticleEntity> articles = page.getArticles();
        if (articles == null)
            articles = new ArrayList<ArticleEntity>();
        if (!articles.contains(articleEntity)) {
            articles.add(articleEntity);
            pageRepository.save(page);
        }
    }

    @Override
    public void deleteArticleById(long articleId) {
        articleRepository.delete(articleId);
    }

    @Override
    public void savePageSort(List<Long> order) {
        Iterable<PageEntity> pages = pageRepository.findAll(order);
        for (PageEntity page : pages) {
            page.setOrder(order.indexOf(page.getId()));
        }
    }

    @Override
    public void saveArticleSort(List<Long> order) {
        Iterable<ArticleEntity> articles = articleRepository.findAll(order);
        for (ArticleEntity article : articles) {
            article.setOrder(order.indexOf(article.getId()));
        }
    }

    @Transactional(readOnly = true)
    @Override
    public List<Page> childsOf(Page page) {
        return pageConverter.convert(pageRepository.findByParentIdOrderByOrderAsc(page.getId()));
    }
}
