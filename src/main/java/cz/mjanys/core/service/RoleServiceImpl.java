package cz.mjanys.core.service;

import cz.mjanys.core.repository.RoleRepository;
import cz.mjanys.iface.converter.RoleConverter;
import cz.mjanys.iface.dto.Role;
import cz.mjanys.iface.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Martin Janys
 */
@Transactional(readOnly = true)
@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private RoleConverter roleConverter;

    @Override
    public List<Role> findAllRoles() {
        return roleConverter.convert(roleRepository.findAll());
    }

    @Override
    public Role findByRoleName(String role) {
        return roleConverter.convert(roleRepository.findByRole(role));
    }

}
