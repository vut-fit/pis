package cz.mjanys.core.service;

import cz.mjanys.core.entity.UserEntity;
import cz.mjanys.core.repository.UserRepository;
import cz.mjanys.iface.converter.UserEntityConverter;
import cz.mjanys.iface.converter.UserConverter;
import cz.mjanys.iface.dto.User;
import cz.mjanys.iface.service.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import javax.persistence.EntityNotFoundException;
import java.util.List;

/**
 * @author Martin Janys
 */
@Transactional
@Service
public class UserServiceImpl implements UserService {

    private static final Logger LOG = Logger.getLogger(UserServiceImpl.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserConverter entityToDto;
    @Autowired
    private UserEntityConverter dtoToEntity;

    @Override
    @Transactional(readOnly = true)
    public List<User> findAllUsers() {
        Iterable<UserEntity> entities = userRepository.findAll();

        return entityToDto.convert(entities);
    }

    @Transactional(readOnly = true)
    @Override
    public User findUserById(long id) {
        return entityToDto.convert(userRepository.findById(id));
    }

    @Override
    @Transactional(readOnly = true)
    public User findUserByUsername(String username) {

        UserEntity entity = userRepository.findByUsernameEnabled(username);
        if (entity != null) {
            return entityToDto.convert(entity);
        }

        throw new EntityNotFoundException("User username='" + username + "'");
    }

    @Override
    public User saveUser(User dto) {
        Assert.notNull(dto);

        UserEntity entity = dtoToEntity.convert(dto);
        return saveUserEntity(entity);
    }

    private User saveUserEntity(UserEntity entity) {
        UserEntity savedEntity = userRepository.save(entity);
        return entityToDto.convert(savedEntity);
    }

    @Override
    public void deleteUserByUsername(String username) {
        UserEntity entity = userRepository.findByUsernameEnabled(username);
        userRepository.delete(entity);
    }

    @Override
    public void deleteUserById(long id) {
        UserEntity entity = userRepository.findOne(id);
        userRepository.delete(entity);
    }

    @Transactional(readOnly = true)
    @Override
    public long countByUsername(String username) {
        return userRepository.countByUsername(username);
    }
}
