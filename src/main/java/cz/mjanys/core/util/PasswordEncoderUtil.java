package cz.mjanys.core.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

/**
 * @author Martin Janys
 */
@Component
public class PasswordEncoderUtil {

    private static PasswordEncoder passwordEncoder;

    @Autowired
    public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
        PasswordEncoderUtil.passwordEncoder = passwordEncoder;
    }

    public static String encode(String input) {
        return passwordEncoder.encode(input);
    }
}
