package cz.mjanys.iface;

/**
 * @author Martin Janys
 */
public class DataLengthConstants {

    public static final int STRING_SHORT = 64;
    public static final int STRING_NORMAL = 255;

}
