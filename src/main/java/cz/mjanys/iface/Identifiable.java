package cz.mjanys.iface;

import java.io.Serializable;

/**
 * @author Martin Janys
 */
public interface Identifiable<T> extends Serializable {

    T getId();

}
