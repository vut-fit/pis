package cz.mjanys.iface.converter;

import cz.mjanys.core.entity.ArticleEntity;
import cz.mjanys.iface.converter.generic.AbstractExtendedConverter;
import cz.mjanys.iface.dto.Article  ;
import org.springframework.stereotype.Component;

/**
 * @author Martin Janys
 */
@Component
public class ArticleConverter extends AbstractExtendedConverter<ArticleEntity, Article> {

    @Override
    public Article newTarget() {
        return new Article();
    }

    @Override
    public Article convert(ArticleEntity source, Article target) {
        target.setId(source.getId());
        target.setArticleType(source.getArticleType());
        target.setTitle(source.getTitle());
        target.setContent(source.getContent());
        target.setAction(source.getAction());
        target.setOrder(source.getOrder());
        target.setAuthor(source.getAuthor());
        target.setPageId(source.getPageId());
        return target;
    }
}
