package cz.mjanys.iface.converter;

import cz.mjanys.core.entity.ArticleEntity;
import cz.mjanys.iface.converter.generic.AbstractExtendedConverter;
import cz.mjanys.iface.dto.Article;
import cz.mjanys.web.layout.LayoutConstants;
import org.springframework.stereotype.Component;

/**
 * @author Martin Janys
 */
@Component
public class ArticleEntityConverter extends AbstractExtendedConverter<Article, ArticleEntity> {

    @Override
    public ArticleEntity newTarget() {
        return new ArticleEntity();
    }

    @Override
    public ArticleEntity convert(Article source, ArticleEntity target) {
        target.setId(source.getId());
        target.setArticleType(source.getArticleType());
        target.setTitle(source.getTitle());
        if (source.getArticleType().equals(LayoutConstants.ARTICLE))
            target.setContent(source.getContent());
        if (source.getArticleType().equals(LayoutConstants.ACTION))
            target.setAction(source.getAction());
        target.setAuthor(source.getAuthor());
        target.setDisplayType(source.getDisplayType());
        target.setOrder(source.getOrder());
        target.setPageId(source.getPageId());
        return target;
    }
}
