package cz.mjanys.iface.converter;

import cz.mjanys.core.entity.PageEntity;
import cz.mjanys.iface.converter.generic.AbstractExtendedConverter;
import cz.mjanys.iface.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

/**
 * @author Martin Janys
 */
@Component
public class PageConverter extends AbstractExtendedConverter<PageEntity, Page> {

    @Autowired
    private ArticleConverter articleConverter;

    @Override
    public Page newTarget() {
        return new Page();
    }

    @Override
    public Page convert(PageEntity source, Page target) {
        Assert.notNull(source);
        Assert.notNull(target);

        target.setId(source.getId());
        target.setTitle(source.getTitle());
        target.setFriendlyUrl(source.getFriendlyUrl());
        target.setOrder(source.getOrder());
        if (source.getArticles() != null)
            target.setArticles(articleConverter.convert(source.getArticles()));
        target.setLayout(source.getLayout());
        target.setArticleLayout(source.getArticleLayout());
        target.setVisible(source.isVisible());
        if (source.getParent() != null) {
            target.setParent(convert(source.getParent()));
        }
        else {
            target.setParent(new Page() {{ // TODO
                setRoot(true);
                setId(0l);
            }});
        }
        return target;

    }
}
