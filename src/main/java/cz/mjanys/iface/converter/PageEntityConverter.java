package cz.mjanys.iface.converter;

import cz.mjanys.core.database.EntityReferenceFactory;
import cz.mjanys.core.entity.ArticleEntity;
import cz.mjanys.core.entity.PageEntity;
import cz.mjanys.iface.converter.generic.AbstractExtendedConverter;
import cz.mjanys.iface.dto.Article;
import cz.mjanys.iface.dto.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

/**
 * @author Martin Janys
 */
@Component
public class PageEntityConverter extends AbstractExtendedConverter<Page, PageEntity> {

    @Autowired
    private EntityReferenceFactory referenceFactory;

    @Override
    public PageEntity newTarget() {
        return new PageEntity();
    }

    @Override
    public PageEntity convert(Page source, PageEntity target) {
        Assert.notNull(source);
        Assert.notNull(target);

        target.setId(source.getId());
        target.setTitle(source.getTitle());
        target.setFriendlyUrl(source.getFriendlyUrl());
        target.setOrder(source.getOrder());
        target.setArticles(referenceFactory.getReferences(ArticleEntity.class, source.getArticles()));
        target.setLayout(source.getLayout());
        target.setArticleLayout(source.getArticleLayout());
        target.setVisible(source.isVisible());
        Long parentId = source.getParent().getId();
        if (parentId != null && parentId != 0)
            target.setParent(referenceFactory.getReference(PageEntity.class, parentId));
        target.setChilds(referenceFactory.getReferences(PageEntity.class, source.getChilds()));
        return target;
    }
}
