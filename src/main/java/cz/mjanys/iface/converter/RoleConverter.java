package cz.mjanys.iface.converter;

import cz.mjanys.core.entity.RoleEntity;
import cz.mjanys.iface.converter.generic.AbstractExtendedConverter;
import cz.mjanys.iface.dto.Role;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

/**
 * @author Martin Janys
 */
@Component
public class RoleConverter extends AbstractExtendedConverter<RoleEntity, Role> {

    @Override
    public Role newTarget() {
        return new Role();
    }

    @Override
    public Role convert(RoleEntity source, Role target) {
        Assert.notNull(source);
        Assert.notNull(target);

        target.setId(source.getId());
        target.setRole(source.getRole());
        return target;
    }
}
