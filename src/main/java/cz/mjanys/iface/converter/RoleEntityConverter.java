package cz.mjanys.iface.converter;

import cz.mjanys.core.entity.RoleEntity;
import cz.mjanys.iface.converter.generic.AbstractExtendedConverter;
import cz.mjanys.iface.dto.Role;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

/**
 * @author Martin Janys
 */
@Component
public class RoleEntityConverter extends AbstractExtendedConverter<Role, RoleEntity> {
    @Override
    public RoleEntity newTarget() {
        return new RoleEntity();
    }

    @Override
    public RoleEntity convert(Role source, RoleEntity target) {
        Assert.notNull(source);
        Assert.notNull(target);

        target.setRole(source.getRole());
        return target;
    }
}
