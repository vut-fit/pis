package cz.mjanys.iface.converter;

import com.google.common.collect.Sets;
import cz.mjanys.core.entity.UserEntity;
import cz.mjanys.iface.converter.generic.AbstractExtendedConverter;
import cz.mjanys.iface.dto.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

/**
 * @author Martin Janys
 */
@Component
public class UserConverter extends AbstractExtendedConverter<UserEntity, User> {

    @Autowired
    private RoleConverter roleConverter;

    @Override
    public User newTarget() {
        return new User();
    }

    @Override
    public User convert(UserEntity source, User target) {
        Assert.notNull(source);
        Assert.notNull(target);

        target.setId(source.getId());
        target.setUsername(source.getUsername());
        target.setPassword(source.getPassword());
        target.setEnable(source.isEnable());
        target.setRoles(Sets.newHashSet(roleConverter.convert(source.getRoles())));
        return target;
    }

}
