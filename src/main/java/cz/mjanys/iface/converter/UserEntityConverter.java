package cz.mjanys.iface.converter;

import com.google.common.collect.Sets;
import cz.mjanys.core.database.EntityReferenceFactory;
import cz.mjanys.core.entity.RoleEntity;
import cz.mjanys.core.entity.UserEntity;
import cz.mjanys.core.util.PasswordEncoderUtil;
import cz.mjanys.iface.converter.generic.AbstractExtendedConverter;
import cz.mjanys.iface.dto.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

/**
 * @author Martin Janys
 */
@Component
public class UserEntityConverter extends AbstractExtendedConverter<User, UserEntity> {

    @Autowired
    private RoleEntityConverter roleEntityConverter;

    @Autowired
    private EntityReferenceFactory references;

    @Override
    public UserEntity newTarget() {
        return new UserEntity();
    }

    @Override
    public UserEntity convert(User source, UserEntity target) {
        Assert.notNull(source);
        Assert.notNull(target);

        target.setId(source.getId());
        target.setUsername(source.getUsername());
        if (StringUtils.hasText(source.getPassword()))
            target.setPassword(PasswordEncoderUtil.encode(source.getPassword()));
        target.setEnable(source.isEnable());
        target.setRoles(Sets.newHashSet(references.getReferences(RoleEntity.class, source.getRoles())));
        return target;
    }

}
