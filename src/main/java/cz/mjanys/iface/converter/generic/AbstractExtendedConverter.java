package cz.mjanys.iface.converter.generic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Martin Janys
 */
public abstract class AbstractExtendedConverter<S, T> implements ExtendedConverter<S, T> {

    @Override
    public List<T> convert(Iterable<S> source) {
        if (source != null) {
            List<T> t = new ArrayList<T>();
            for (S s : source) {
                t.add(convert(s));
            }
            return t;
        }
        else {
            return Collections.emptyList();
        }
    }

    @Override
    public T convert(S source) {
        return convert(source, newTarget());
    }

}
