package cz.mjanys.iface.dto;

import org.springframework.util.Assert;

/**
 * @author Martin Janys
 */
public class Layout implements Comparable<Layout> {

    private final String layout;
    private final String layoutName;

    public Layout(String layoutName, String layout) {
        Assert.notNull(layoutName);
        Assert.notNull(layout);
        this.layoutName = layoutName;
        this.layout = layout;
    }

    public String getLayout() {
        return layout;
    }

    public String getLayoutName() {
        return layoutName;
    }

    @Override
    public int compareTo(Layout o) {
        return layoutName.compareTo(o.layoutName);
    }
}
