package cz.mjanys.iface.dto;

import java.util.List;

/**
 * @author Martin Janys
 */
public class RootPage extends HirearchialPage {

    public RootPage(List<Page> childs) {
        super(true);
        setId(0l);
        setChilds(childs);
        setParent(new Page(){{ // TODO
            setId(0l);
        }});
    }
}
