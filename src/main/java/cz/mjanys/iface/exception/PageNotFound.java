package cz.mjanys.iface.exception;

/**
 * @author Martin Janys
 */
public class PageNotFound extends RuntimeException {

    public PageNotFound() {
    }

    public PageNotFound(String message) {
        super(message);
    }

    public PageNotFound(String message, Throwable cause) {
        super(message, cause);
    }

    public PageNotFound(Throwable cause) {
        super(cause);
    }

    public PageNotFound(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
