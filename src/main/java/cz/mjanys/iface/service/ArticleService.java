package cz.mjanys.iface.service;

import cz.mjanys.iface.dto.Article;

/**
 * @author Martin Janys
 */
public interface ArticleService {

    Article findArticleById(long id);

}
