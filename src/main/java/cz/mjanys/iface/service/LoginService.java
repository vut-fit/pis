package cz.mjanys.iface.service;

import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * @author Martin Janys
 */
public interface LoginService extends UserDetailsService {
}
