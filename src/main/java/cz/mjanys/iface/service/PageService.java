package cz.mjanys.iface.service;

import cz.mjanys.iface.dto.Article;
import cz.mjanys.iface.dto.HirearchialPage;
import cz.mjanys.iface.dto.Page;
import cz.mjanys.iface.dto.RootPage;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author Martin Janys
 */
public interface PageService {

    List<Page> findAllPages();

    long countPages();

    RootPage getRootPage();

    RootPage getPageTree();

    Page getPageSubtreeTree(long id);

    Page findPageById(long id);

    Page findPageByFriendlyUrl(String friendlyUrl);

    HirearchialPage findPageByRequest(HttpServletRequest request);

    Page savePage(Page page);

    void deletePageById(long id);

    void saveArticle(Article article);

    void deleteArticleById(long articleId);

    void savePageSort(List<Long> order);

    void saveArticleSort(List<Long> order);

    Object childsOf(Page page);
}
