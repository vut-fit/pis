package cz.mjanys.iface.service;

import cz.mjanys.iface.dto.Role;

import java.util.List;

/**
 * @author Martin Janys
 */
public interface RoleService {

    List<Role> findAllRoles();

    Role findByRoleName(String role);

}
