package cz.mjanys.iface.service;

import cz.mjanys.iface.dto.User;

import java.util.List;

/**
 * @author Martin Janys
 */
public interface UserService {

    List<User> findAllUsers();

    User findUserById(long id);

    User findUserByUsername(String username);

    User saveUser(User dto);

    void deleteUserByUsername(String username);

    void deleteUserById(long id);

    long countByUsername(String username);
}
