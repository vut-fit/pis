package cz.mjanys.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.*;

import static cz.mjanys.web.Constants.*;

/**
 * @author Martin Janys
 */
@Controller
public class AbstractController {

    @Autowired
    protected MessageSource messageSource;

    @Autowired
    protected ServletContext servletContext;

    private static final String SUCCESS = "success";
    private static final String ERROR = "danger";
    private static final String INFO = "info";
    private static final String WARNING = "warning";

    public static enum MessageType {

        SUCCESS(AbstractController.SUCCESS),
        ERROR(AbstractController.ERROR),
        INFO(AbstractController.INFO),
        WARNING(AbstractController.WARNING);

        private final String value;

        MessageType(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    public static final String MESSAGES = "flashMessages";

    protected String redirect(String location) {
        return REDIRECT_PREFIX + location;
    }

    protected String redirectView(String location) {
        return REDIRECT_VIEW_PREFIX + location;
    }

    @SuppressWarnings("unchecked")
    private Map<String, List<String>> getMessages(Model model) {
        Map<String, Object> modelMap = model.asMap();
        Object messages = modelMap.get(MESSAGES);
        if (messages != null)
            return (Map<String, List<String>>) messages;
        model.addAttribute(MESSAGES, createEmptyMessages());
        return (Map<String, List<String>>) modelMap.get(MESSAGES);
    }

    private Map<String, List<String>> getMessages(HttpSession request) {
        Object messages = request.getAttribute(MESSAGES);
        if (messages != null)
            return (Map<String, List<String>>) messages;
        request.setAttribute(MESSAGES, createEmptyMessages());
        return (Map<String, List<String>>) request.getAttribute(MESSAGES);
    }

    private Map<String, List<String>> createEmptyMessages() {
        Map<String, List<String>> messages = new HashMap<String, List<String>>(3);
        for (MessageType messageType : MessageType.values()) {
            messages.put(messageType.getValue(), new ArrayList<String>());
        }
        return messages;
    }

    protected void addFlashMessage(MessageType messageType, Model model, String message, String ... args) {
        Map<String, List<String>> messages = getMessages(model);
        if (messages == null) {
            messages = createEmptyMessages();
            model.addAttribute(MESSAGES, message);
        }
        List<String> messageList = messages.get(messageType.getValue());
        messageList.add(messageBundle(message, args));
    }

    protected void addFlashMessage(MessageType messageType, HttpServletRequest request, String message, String ... args) {
        HttpSession session = request.getSession();
        Map<String, List<String>> messages = getMessages(session);
        if (messages == null) {
            messages = createEmptyMessages();
            session.setAttribute(MESSAGES, message);
        }
        List<String> messageList = messages.get(messageType.getValue());
        messageList.add(messageBundle(message, args));
    }

    private String messageBundle(String message, String ... args) {
        return messageSource.getMessage(message, args, Locale.getDefault());
    }

    protected void addSuccessMessage(HttpServletRequest request, String message, String ... args) {
        addFlashMessage(MessageType.SUCCESS, request, message, args);
    }

    protected void addInfoMessage(HttpServletRequest request, String message, String ... args) {
        addFlashMessage(MessageType.INFO, request, message, args);
    }

    protected void addErrorMessage(HttpServletRequest request, String message, String ... args) {
        addFlashMessage(MessageType.ERROR, request, message, args);
    }

    protected void addWarningMessage(Model model, String message, String ... args) {
        addFlashMessage(MessageType.WARNING, model, message, args);
    }

    protected String contextPath() {
        String path = servletContext.getContextPath();
        if (path == null || path.isEmpty()) {
            return "/";
        }
        else {
            return path;
        }
    }

}
