package cz.mjanys.web;

/**
 * @author Martin Janys
 */
public class Constants {

    public static final String DATE_TIME_FORMAT = "dd.MM.yyyy HH:mm:ss";

    public static final String REDIRECT_PREFIX = "redirect:";
    public static final String REDIRECT_VIEW_PREFIX = "redirectView:";

    public static final String PARAM_CONTROLLER = "controller";
    public static final String PARAM_PAGE = "page";
    public static final String PARAM_PAGE_ID = "pageId";
    public static final String PARAM_ARTICLE_ID = "articleId";
    public static final String PARAM_ID = "id";
    public static final String PARAM_USERNAME = "username";
    public static final String PARAM_PARENT_ID = "parentId";

    public static final String VIEW_INDEX = "index";
    public static final String VIEW_NO_PAGES = "layout/no_pages";

    public static final String VIEW_403 = "error/403";

    public static final String PAGE_TO_PAGE = "/page/{id}";
    public static final String PAGE_NO_PAGES = "/nopages";
    public static final String PAGE_ERROR_403 = "/error/403";

    public static final String ATTR_REDIRECT = "redirect";

    public static final String VIEW_AJAX_REDIRECT = "ajax_redirect";

}
