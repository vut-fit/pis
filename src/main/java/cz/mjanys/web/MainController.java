package cz.mjanys.web;

import cz.mjanys.iface.dto.Page;
import cz.mjanys.iface.service.PageService;
import cz.mjanys.web.layout.LayoutConstants;
import cz.mjanys.web.page.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static cz.mjanys.web.Constants.*;

/**
 * @author Martin Janys
 */
@Controller
@RequestMapping("/")
public class MainController extends AbstractController {

    @Autowired
    private PageService pageService;

    @Autowired
    private PageHelper pageHelper;

    @RequestMapping
    public void index(
            HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

        if (pageService.countPages() == 0) {
            RequestDispatcher requestDispatcher = servletContext.getRequestDispatcher(PAGE_NO_PAGES);
            requestDispatcher.forward(request, response);
        }
        else {
            Page page = pageService.getRootPage().getChilds().get(0);
            request.setAttribute(LayoutConstants.ATTR_CURRENT_PAGE, pageService.getRootPage());
            RequestDispatcher requestDispatcher = servletContext.getRequestDispatcher(pageHelper.pageUri(page.getFriendlyUrl()));
            requestDispatcher.forward(request, response);
        }
    }

    @RequestMapping(PAGE_NO_PAGES)
    public String noPages(HttpServletRequest request) {
        return VIEW_NO_PAGES;
    }

    @RequestMapping(PAGE_ERROR_403)
    public String accessDennie() {
        return VIEW_403;
    }

    @RequestMapping(PAGE_TO_PAGE)
    public void toPage(
            @RequestParam(PARAM_ID) Long id,
            HttpServletResponse response) throws IOException {
        Page page = pageService.findPageById(id);
        response.sendRedirect(servletContext.getContextPath() + pageHelper.pageUri(servletContext.getContextPath(), page.getFriendlyUrl()));
    }
}
