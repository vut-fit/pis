package cz.mjanys.web.admin;

/**
 * @author Martin Janys
 */
public class AdminConstants {

    public static final String ADMIN_MAPPING = "/admin";

    public static final String VIEW = "admin/view";

}
