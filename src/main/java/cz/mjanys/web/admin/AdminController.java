package cz.mjanys.web.admin;

import cz.mjanys.web.user.UserConstants;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.ServletContextAware;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

import static cz.mjanys.web.admin.AdminConstants.*;

/**
 * @author Martin Janys
 */
@Controller
@RequestMapping(ADMIN_MAPPING)
public class AdminController implements ServletContextAware {

    private ServletContext servletContext;

    @Override
    public void setServletContext(ServletContext servletContext) {
        this.servletContext = servletContext;
    }

    @RequestMapping
    public void view(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = servletContext.getRequestDispatcher(UserConstants.USER_MAPPING);
        requestDispatcher.include(request, response);
    }
}
