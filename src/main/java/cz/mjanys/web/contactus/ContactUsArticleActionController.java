package cz.mjanys.web.contactus;

import cz.mjanys.web.AbstractController;
import cz.mjanys.web.contactus.pto.ContactUsPto;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import java.io.IOException;

import static cz.mjanys.web.contactus.ContactUsConstants.*;

/**
 * @author Martin Janys
 */
@Controller
@RequestMapping(CONTACT_US_MAPPING)
public class ContactUsArticleActionController extends AbstractController {

    @ModelAttribute(ATTR_CONTACT_US_PTO)
    protected ContactUsPto contactUsPto() {
        return new ContactUsPto();
    }

    @RequestMapping
    public String view(
            HttpServletRequest request,
            HttpServletResponse response) throws IOException {
        return VIEW;
    }

    @RequestMapping(method = RequestMethod.POST)
    public String action(
            HttpServletRequest request,
            HttpServletResponse response,
            @Valid @ModelAttribute(ATTR_CONTACT_US_PTO) ContactUsPto contactUsPto,
            BindingResult result,
            Model model) {
        if (!result.hasErrors()) {
            addSuccessMessage(request, "success");
            return VIEW_STATUS;
        }
        else {
            return VIEW;
        }
    }

}
