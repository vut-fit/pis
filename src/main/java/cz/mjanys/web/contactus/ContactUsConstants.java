package cz.mjanys.web.contactus;

import cz.mjanys.web.Constants;

/**
 * @author Martin Janys
 */
public class ContactUsConstants extends Constants {

    public static final String CONTACT_US_MAPPING = "/contact-us";

    public static final String ATTR_CONTACT_US_PTO = "contactUsPto";

    public static final String VIEW = "contactus/view";
    public static final String VIEW_STATUS = "contactus/view_status";

}
