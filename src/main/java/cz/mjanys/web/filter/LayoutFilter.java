package cz.mjanys.web.filter;

import cz.mjanys.iface.dto.HirearchialPage;
import cz.mjanys.iface.dto.RootPage;
import cz.mjanys.iface.service.PageService;
import cz.mjanys.web.layout.LayoutConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * @author Martin Janys
 */
@SuppressWarnings("SpringJavaAutowiredMembersInspection")
public class LayoutFilter implements Filter {

    @Autowired
    private PageService pageService;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        ServletContext servletContext = filterConfig.getServletContext();
        WebApplicationContext wac = WebApplicationContextUtils.getWebApplicationContext(servletContext);

        AutowireCapableBeanFactory autowireCapableBeanFactory = wac.getAutowireCapableBeanFactory();
        autowireCapableBeanFactory.autowireBean(this);
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HirearchialPage page = pageService.findPageByRequest((HttpServletRequest) request);
        RootPage rootPage = pageService.getPageTree();

        request.setAttribute(LayoutConstants.ATTR_CURRENT_PAGE, page);
        request.setAttribute(LayoutConstants.ATTR_ROOT_PAGE, rootPage);

        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }
}
