package cz.mjanys.web.http;

import javax.servlet.ServletOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * @author Martin Janys
 */
public class ByteArrayServletOutputStream extends ServletOutputStream {

    private final ByteArrayOutputStream outputStream;

    public ByteArrayServletOutputStream() {
        outputStream = new ByteArrayOutputStream();
    }

    @Override
    public void write(int b) throws IOException {
        outputStream.write(b);
    }

    public byte[] toByteArray() {
       return outputStream.toByteArray();
    }

}
