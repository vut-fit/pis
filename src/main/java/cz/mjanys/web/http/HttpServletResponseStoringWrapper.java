package cz.mjanys.web.http;

import javax.servlet.ServletOutputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author Martin Janys
 */
public class HttpServletResponseStoringWrapper extends HttpServletResponseWrapper {

    private boolean committed;
    private boolean writerInUse;
    private boolean outputStreamInUse;

    private ByteArrayServletOutputStream outputStream;
    private ByteArrayOutputStream printStream;
    private PrintWriter printWriter;


    /**
     * Constructs a response adaptor wrapping the given response.
     *
     * @param response
     * @throws IllegalArgumentException if the response is null
     */
    public HttpServletResponseStoringWrapper(HttpServletResponse response) {
        super(response);
    }

    public static HttpServletResponseStoringWrapper wrap(HttpServletResponse response) {
        return new HttpServletResponseStoringWrapper(response);
    }

    public byte[] toByteArrayResponse() throws IOException {
        if (outputStream != null) {
            outputStream.flush();
            return outputStream.toByteArray();
        }
        if (printWriter != null) {
            printWriter.flush();
            return printStream.toByteArray();
        }

        return null;
    }

    @Override
    public ServletOutputStream getOutputStream() throws IOException {
        if (writerInUse) {
            throw new IllegalStateException("Writer is already in use");
        }

        outputStreamInUse = true;

        if (outputStream == null)
            outputStream = new ByteArrayServletOutputStream();
        return outputStream;
    }

    @Override
    public PrintWriter getWriter() throws IOException {
        if (outputStreamInUse) {
            throw new IllegalStateException("Writer is already in use");
        }

        writerInUse = true;

        if (printWriter == null) {
            printStream = new ByteArrayOutputStream();
            printWriter = new PrintWriter(printStream);
        }
        return printWriter;
    }
}
