package cz.mjanys.web.layout;

import cz.mjanys.web.Constants;

/**
 * @author Martin Janys
 */
public class LayoutConstants extends Constants {

    public static final String LAYOUT_MAPPING = "/layout";

    public static final String PAGE_ADD_PAGE = "/add_page";
    public static final String PAGE_REMOVE_PAGE = "/remove_page";
    public static final String PAGE_EDIT_PAGE = "/edit_page";
    public static final String PAGE_MANAGE_PAGES = "/manage_pages";
    public static final String ACTION_SAVE_PAGE = "/save_page";
    public static final String ACTION_SAVE_ARTICLE = "/save_article";
    public static final String ACTION_DELETE_ARTICLE = "/delete_article";
    public static final String ACTION_ORDER_PAGES = "/order_pages";
    public static final String ACTION_ORDER_ARTICLES = "/order_articles";
    public static final String SERVE_PAGE_TREE = "/serve_page_tree";
    public static final String SERVE_EDIT_ARTICLE = "/serve_edit_article";

    public static final String VIEW_ADD_PAGE = "layout/view_add_page";
    public static final String VIEW_EDIT_PAGE = "layout/view_edit_page";
    public static final String VIEW_MANAGE_PAGES = "layout/view_manage_pages";
    public static final String VIEW_ARTICLE_DIALOG = "article/view_article_dialog";

    public static final String PARAM_PARENT_PAGE = "parentPage";
    public static final String PARAM_ORDER_PAGES = "orderPages";

    public static final String ATTR_CURRENT_PAGE = "currentPage";
    public static final String ATTR_PAGE_PTO = "pagePto";
    public static final String ATTR_PAGE = "page";
    public static final String ATTR_PAGES = "pages";
    public static final String ATTR_PAGE_LAYOUTS = "pageLayouts";
    public static final String ATTR_ARTICLE_LAYOUTS = "articleLayouts";
    public static final String ATTR_ROOT_PAGE = "rootPage";
    public static final String ATTR_ARTICLE = "article";
    public static final String ATTR_TREE = "tree";
    public static final String ATTR_ORDER = "order";

    public static final String ARTICLE = "article";
    public static final String ACTION = "action";
}
