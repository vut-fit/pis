package cz.mjanys.web.layout;

import cz.mjanys.iface.dto.Article;
import cz.mjanys.iface.dto.Layout;
import cz.mjanys.iface.dto.Page;
import cz.mjanys.iface.service.PageService;
import cz.mjanys.web.AbstractController;
import cz.mjanys.web.layout.converter.PagePtoToDtoConverter;
import cz.mjanys.web.layout.converter.PageToPagePtoConverter;
import cz.mjanys.web.layout.pto.OrderPto;
import cz.mjanys.web.layout.pto.PagePto;
import cz.mjanys.web.layout.pto.PageTreeNode;
import cz.mjanys.web.layout.validator.ArticleValidator;
import cz.mjanys.web.layout.validator.PagePtoValidator;
import cz.mjanys.web.page.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.security.Principal;
import java.util.Collections;
import java.util.List;

import static cz.mjanys.web.layout.LayoutConstants.*;

/**
 * @author Martin Janys
 */
@Controller
@RequestMapping(LAYOUT_MAPPING)
public class LayoutController extends AbstractController {

    @Autowired
    private PageService pageService;

    @Autowired
    private PagePtoToDtoConverter pagePtoToDto;
    @Autowired
    private PageToPagePtoConverter pageToPagePto;

    @Autowired
    private PagePtoValidator pagePtoValidator;

    @Autowired
    private ArticleValidator articleValidator;

    @Autowired
    private PageHelper pageHelper;

    @Value("${page-layout-dir}")
    private String pageLayoutDir;

    @Value("${article-layout-dir}")
    private String articleLayoutDir;

    @InitBinder(ATTR_PAGE_PTO)
    protected void initBinderPagePto(WebDataBinder webDataBinder) {
        webDataBinder.addValidators(pagePtoValidator);
    }

    @InitBinder(ATTR_ARTICLE)
    protected void initBinderArticle(WebDataBinder webDataBinder) {
        webDataBinder.addValidators(articleValidator);
    }

    @ModelAttribute(ATTR_PAGE_PTO)
    protected PagePto page() {
        return new PagePto();
    }

    @ModelAttribute(ATTR_PAGE_LAYOUTS)
    protected List<Layout> pageLayouts() {
        String layoutDir = servletContext.getRealPath(pageLayoutDir);
        return LayoutHelper.getAvailableLayouts(layoutDir, messageSource, LocaleContextHolder.getLocale());
    }

    @ModelAttribute(ATTR_ARTICLE_LAYOUTS)
    protected List<Layout> articleLayouts() {
        String layoutDir = servletContext.getRealPath(articleLayoutDir);
        return LayoutHelper.getAvailableLayouts(layoutDir, messageSource, LocaleContextHolder.getLocale());
    }

    @ModelAttribute(ATTR_PAGES)
    protected List<Page> pages() {
        return getPagesToSelect();
    }

    @RequestMapping(PAGE_ADD_PAGE)
    public String addPage(
            @RequestParam(PARAM_PARENT_ID) Long parentId,
            @ModelAttribute(ATTR_PAGE_PTO) PagePto pagePto,
            Model model) {

        pagePto.setParentId(parentId);

        return VIEW_ADD_PAGE;
    }

    public List<Page> getPagesToSelect() {
        List<Page> pages = pageService.findAllPages();
        pages.add(0, createDefaultPage());
        return pages;
    }

    private Page createDefaultPage() {
        Page page = new Page();
        page.setId(0l);
        page.setTitle(messageSource.getMessage("label-root-page", null, LocaleContextHolder.getLocale()));
        return page;
    }

    @RequestMapping(PAGE_EDIT_PAGE)
    public String editPage(
            @RequestParam(PARAM_PAGE_ID) long pageId,
            Model model,
            HttpServletRequest request,
            HttpServletResponse response) {

        Page page = pageService.findPageById(pageId);
        Assert.notNull(page);

        model.addAttribute(ATTR_PAGE_PTO, pageToPagePto.convert(page));
        model.addAttribute(ATTR_PAGES, getPagesToSelect());

        return VIEW_EDIT_PAGE;
    }

    @RequestMapping(PAGE_REMOVE_PAGE)
    public void removePage(
            @RequestParam(PARAM_PAGE_ID) long pageId,
            HttpServletRequest request,
            HttpServletResponse response
    ) throws IOException {
        Page page = pageService.findPageById(pageId);
        Assert.notNull(page);

        sendRedirectToParentPage(page, response);

        pageService.deletePageById(page.getId());
    }

    private void sendRedirectToParentPage(Page page, HttpServletResponse response) throws IOException {
        Page parent = page.getParent();
        if (parent != null && parent.getId() != 0l) { // todo: jen null/id
            String friendlyUrl = parent.getFriendlyUrl();
            response.sendRedirect(pageHelper.pageUri(servletContext.getContextPath(), friendlyUrl));
        }
        else {
            response.sendRedirect(contextPath());
        }
    }

    @RequestMapping(PAGE_MANAGE_PAGES)
    public String managePages(Model model) throws Exception {
        model.addAttribute(ATTR_TREE, LayoutHelper.makePageTree(
                servletContext.getContextPath(),
                messageSource.getMessage("label-pages-root", null, LocaleContextHolder.getLocale()),
                pageService.getPageTree()
        ));
        return VIEW_MANAGE_PAGES;
    }

    @RequestMapping(value = ACTION_SAVE_PAGE, method = RequestMethod.POST)
    public String savePage(
            @Valid @ModelAttribute(ATTR_PAGE_PTO) PagePto pagePto,
            BindingResult result,
            HttpServletRequest request,
            HttpServletResponse response
    ) throws IOException {

        if (!result.hasErrors()) {
            Page page = pagePtoToDto.convert(pagePto);
            page = pageService.savePage(page);
            Assert.notNull(page.getFriendlyUrl());
            response.sendRedirect(pageHelper.pageUri(servletContext.getContextPath(), page.getFriendlyUrl()));
            return null;
        }
        else {
            return VIEW_ADD_PAGE;
        }
    }

    @RequestMapping(value = ACTION_ORDER_PAGES, method = RequestMethod.POST)
    public void orderPages(
            @ModelAttribute(ATTR_ORDER) OrderPto orderPto,
            HttpServletResponse response) {

        pageService.savePageSort(orderPto.getOrder());

        response.setStatus(HttpServletResponse.SC_OK);
    }

    @RequestMapping(value = ACTION_ORDER_ARTICLES, method = RequestMethod.POST)
    public void orderAricles(
            @ModelAttribute(ATTR_ORDER) OrderPto orderPto,
            HttpServletResponse response) {

        pageService.saveArticleSort(orderPto.getOrder());

        response.setStatus(HttpServletResponse.SC_OK);
    }

    @RequestMapping(value = SERVE_PAGE_TREE, headers = "Accept=*/*")
    @ResponseBody
    public List<PageTreeNode> servePageTree() throws Exception {
        return Collections.singletonList(
                LayoutHelper.makePageTree(
                        servletContext.getContextPath(),
                        messageSource.getMessage("label-pages-root", null, LocaleContextHolder.getLocale()),
                        pageService.getPageTree()
                )
        );
    }

    @RequestMapping(value = ACTION_SAVE_ARTICLE, method = RequestMethod.POST)
    public String saveArticle(
            @RequestParam(PARAM_PAGE_ID) Page page,
            @Valid @ModelAttribute(ATTR_ARTICLE) Article article,
            BindingResult result,
            Model model,
            HttpServletRequest request,
            HttpServletResponse response,
            Principal principal) throws IOException, ServletException {

        if (!result.hasErrors()) {
            article.setAuthor(principal.getName());
            pageService.saveArticle(article);

            String redirectUrl = pageHelper.pageUri(servletContext.getContextPath(), page.getFriendlyUrl());
            model.addAttribute(ATTR_REDIRECT, redirectUrl);
            return VIEW_AJAX_REDIRECT;
        }
        else {
            return VIEW_ARTICLE_DIALOG;
        }
    }

    @RequestMapping(ACTION_DELETE_ARTICLE)
    public void deleteArticle(
            @RequestParam(PARAM_PAGE_ID) Page page,
            @RequestParam(PARAM_ARTICLE_ID) Long articleId,
            HttpServletRequest request,
            HttpServletResponse response) throws IOException {

        pageService.deleteArticleById(articleId);

        response.sendRedirect(pageHelper.pageUri(servletContext.getContextPath(), page.getFriendlyUrl()));
    }

    @RequestMapping(SERVE_EDIT_ARTICLE)
    public String serveEditArticle(
            @RequestParam(PARAM_ARTICLE_ID) Article article,
            Model model) {

        model.addAttribute(ATTR_ARTICLE, article);

        return VIEW_ARTICLE_DIALOG;
    }
}
