package cz.mjanys.web.layout;

import cz.mjanys.iface.dto.Layout;
import cz.mjanys.iface.dto.Page;
import cz.mjanys.iface.dto.RootPage;
import cz.mjanys.web.layout.pto.PageTreeNode;
import org.apache.commons.io.FilenameUtils;
import org.apache.http.client.utils.URIBuilder;
import org.springframework.context.MessageSource;
import org.springframework.util.Assert;

import java.io.File;
import java.util.*;

/**
 * @author Martin Janys
 */
public class LayoutHelper {

    public static List<Layout> getAvailableLayouts(String dirName, MessageSource messages, Locale locale) {
        List<Layout> layouts = new ArrayList<Layout>();
        File dir = new File(dirName);
        Assert.state(dir.exists());
        Assert.state(dir.isDirectory());

        for (File file : dir.listFiles()) {
            String fileName = FilenameUtils.removeExtension(file.getName());
            layouts.add(new Layout(
                    messages.getMessage(fileName, null, locale),
                    fileName));
        }

        Collections.sort(layouts);
        return layouts;
    }

    public static PageTreeNode makePageTree(String contextPath, String rootLabel, RootPage pageTree) throws Exception {
        PageTreeNode rootTreeNode = new PageTreeNode();
        rootTreeNode.setText(rootLabel);
        rootTreeNode.setId(0l);

        List<PageTreeNode> nodes = new ArrayList<PageTreeNode>();
        for (Page page : pageTree.getChilds()) {
            nodes.add(makePageTree(page));
        }
        rootTreeNode.setNodes(nodes);
        return rootTreeNode;
    }

    private static PageTreeNode makePageTree(Page page) throws Exception {
        PageTreeNode treeNode = new PageTreeNode();
        treeNode.setText(page.getTitle());
        treeNode.setOrder(page.getOrder());
        treeNode.setId(page.getId());
        treeNode.setVisible(page.isVisible());
        treeNode.setFriendlyUrl(page.getFriendlyUrl());
        List<PageTreeNode> nodes = new ArrayList<PageTreeNode>();
        for (Page p : page.getChilds()) {
            nodes.add(makePageTree(p));
        }
        treeNode.setNodes(nodes.isEmpty() ? null : nodes);
        return treeNode;
    }
}
