package cz.mjanys.web.layout.converter;

import cz.mjanys.iface.converter.generic.AbstractExtendedConverter;
import cz.mjanys.iface.dto.Page;
import cz.mjanys.web.layout.pto.PagePto;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

/**
 * @author Martin Janys
 */
@Component
public class PagePtoToDtoConverter extends AbstractExtendedConverter<PagePto, Page> {

    @Override
    public Page newTarget() {
        return new Page();
    }

    @Override
    public Page convert(final PagePto source, final Page target) {
        Assert.notNull(source);
        Assert.notNull(target);

        target.setId(source.getId());
        target.setTitle(source.getTitle());
        target.setFriendlyUrl(source.getFriendlyUrl());
        target.setOrder(source.getOrder());
        //target.setArticles();
        target.setLayout(source.getLayout());
        target.setArticleLayout(source.getArticleLayout());
        target.setVisible(source.isVisible());
        target.setParent(new Page() {{ // TODO
            setId(source.getParentId());
        }});
        return target;
    }
}
