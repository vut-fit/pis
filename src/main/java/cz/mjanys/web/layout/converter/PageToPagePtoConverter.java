package cz.mjanys.web.layout.converter;

import cz.mjanys.iface.converter.generic.AbstractExtendedConverter;
import cz.mjanys.iface.dto.Page;
import cz.mjanys.web.layout.pto.PagePto;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

/**
 * @author Martin Janys
 */
@Component
public class PageToPagePtoConverter extends AbstractExtendedConverter<Page, PagePto> {

    @Override
    public PagePto newTarget() {
        return new PagePto();
    }

    @Override
    public PagePto convert(Page source, PagePto target) {
        Assert.notNull(source);
        Assert.notNull(target);

        target.setId(source.getId());
        target.setTitle(source.getTitle());
        target.setFriendlyUrl(source.getFriendlyUrl());
        target.setOrder(source.getOrder());
        //target.setArticles();
        target.setLayout(source.getLayout());
        target.setArticleLayout(source.getArticleLayout());
        target.setVisible(source.isVisible());
        Page parent = source.getParent();
        if (parent != null)
            target.setParentId(parent.getId());
        else
            target.setParentId(0l);
        return target;
    }
}
