package cz.mjanys.web.layout.converter;

import cz.mjanys.iface.dto.Article;
import cz.mjanys.iface.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

/**
 * @author Martin Janys
 */
@Component
public class StringToArticleConverter implements Converter<String, Article> {

    @Autowired
    private ArticleService articleService;

    @Override
    public Article convert(String source) {
        Assert.notNull(source);

        return articleService.findArticleById(Long.valueOf(source));
    }
}
