package cz.mjanys.web.layout.converter;

import cz.mjanys.iface.dto.Page;
import cz.mjanys.iface.service.PageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

/**
 * @author Martin Janys
 */
@Component
public class StringToPageConverter implements Converter<String, Page> {

    @Autowired
    private PageService pageService;

    @Override
    public Page convert(String source) {
        Assert.notNull(source);

        if (StringUtils.hasText(source) && !source.equals("0")) {
            return pageService.findPageById(Long.valueOf(source));
        }
        return null;
    }
}
