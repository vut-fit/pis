package cz.mjanys.web.layout.pto;

import java.util.List;

/**
 * @author Martin Janys
 */
public class OrderPto {

    private List<Long> order;

    public List<Long> getOrder() {
        return order;
    }

    public void setOrder(List<Long> order) {
        this.order = order;
    }
}
