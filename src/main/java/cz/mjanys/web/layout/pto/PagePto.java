package cz.mjanys.web.layout.pto;

import cz.mjanys.iface.DataLengthConstants;
import cz.mjanys.iface.dto.Article;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;
import java.util.List;

/**
 * @author Martin Janys
 */
public class PagePto {

    private Long id;
    @NotEmpty
    @Size(max = DataLengthConstants.STRING_SHORT)
    private String title;
    @NotEmpty
    @Size(max = DataLengthConstants.STRING_SHORT)
    private String friendlyUrl;
    private Integer order;
    private List<Article> articles;
    @NotEmpty
    private String layout;
    @NotEmpty
    private String articleLayout;
    private boolean visible = true;
    private Long parentId = 0l;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFriendlyUrl() {
        return friendlyUrl;
    }

    public void setFriendlyUrl(String friendlyUrl) {
        this.friendlyUrl = friendlyUrl;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public List<Article> getArticles() {
        return articles;
    }

    public void setArticles(List<Article> articles) {
        this.articles = articles;
    }

    public String getLayout() {
        return layout;
    }

    public void setLayout(String layout) {
        this.layout = layout;
    }

    public String getArticleLayout() {
        return articleLayout;
    }

    public void setArticleLayout(String articleLayout) {
        this.articleLayout = articleLayout;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }
}
