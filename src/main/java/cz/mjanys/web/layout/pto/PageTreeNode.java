package cz.mjanys.web.layout.pto;

import java.util.List;

/**
 * @author Martin Janys
 */
public class PageTreeNode {

    private Long id;
    private String text;
    private String friendlyUrl;
    private List<PageTreeNode> nodes;
    private Integer order;
    private boolean visible;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<PageTreeNode> getNodes() {
        return nodes;
    }

    public void setNodes(List<PageTreeNode> nodes) {
        this.nodes = nodes;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getFriendlyUrl() {
        return friendlyUrl;
    }

    public void setFriendlyUrl(String friendlyUrl) {
        this.friendlyUrl = friendlyUrl;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }
}
