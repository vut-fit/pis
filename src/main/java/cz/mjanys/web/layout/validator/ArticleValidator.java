package cz.mjanys.web.layout.validator;

import cz.mjanys.iface.dto.Article;
import cz.mjanys.web.layout.LayoutConstants;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * @author Martin Janys
 */
@Component
public class ArticleValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return Article.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Article article = (Article) target;

        String articleType = article.getArticleType();
        if (LayoutConstants.ACTION.equals(articleType)) {
            ValidationUtils.rejectIfEmpty(errors, "action", "msg-error-action-not-empty");
        }
    }
}
