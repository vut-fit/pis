package cz.mjanys.web.layout.validator;

import cz.mjanys.iface.service.PageService;
import cz.mjanys.web.layout.pto.PagePto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * @author Martin Janys
 */
@Component
public class PagePtoValidator implements Validator {

    @Autowired
    private PageService pageService;

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.isAssignableFrom(PagePto.class);
    }

    @Override
    public void validate(Object target, Errors errors) {
        PagePto pagePto = (PagePto) target;

        if (pagePto.getId() == null && pageService.findPageByFriendlyUrl(pagePto.getFriendlyUrl()) != null) {
            errors.rejectValue("friendlyUrl", "msg-err-not-unique-friendlyUrl");
        }

        if (pagePto.getId() != null && pagePto.getId().equals(pagePto.getParentId())) {
            errors.rejectValue("parentId", "msg-err-page-cant-be-parent");
        }
    }
}
