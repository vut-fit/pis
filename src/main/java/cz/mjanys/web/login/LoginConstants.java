package cz.mjanys.web.login;

import cz.mjanys.web.Constants;

/**
 * @author Martin Janys
 */
public class LoginConstants extends Constants {

    public static final String PAGE_LOGIN = "/login";
    public static final String PAGE_LOGIN_ERROR = "login/error";
    public static final String PAGE_LOGOUT = "/logout";
    public static final String PAGE_403 = "/403";

    public static final String ATTR_ERROR = "error";
    public static final String ATTR_MESSAGE = "msg";
    public static final String ATTR_LOGIN_PTO = "loginPto";

    public static final String VIEW_MAIN = "login/view";
    public static final String VIEW_403 = "error/403";

}
