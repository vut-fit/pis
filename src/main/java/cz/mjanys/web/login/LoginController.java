package cz.mjanys.web.login;

import cz.mjanys.web.AbstractController;
import cz.mjanys.web.login.pto.LoginPto;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static cz.mjanys.web.login.LoginConstants.*;

/**
 * @author Martin Janys
 */
@Controller
public class LoginController extends AbstractController {

    @ModelAttribute
    public LoginPto model() {
        return new LoginPto();
    }

    @RequestMapping(PAGE_LOGIN)
    public String login(Model model) {
        return VIEW_MAIN;
    }

    @RequestMapping(PAGE_LOGOUT)
    public String logout(
            HttpServletRequest request,
            HttpServletResponse response,
            Model model) {
        SecurityContextHolder.clearContext();
        addInfoMessage(request, "successful-logout");
        return VIEW_MAIN;
    }

    @RequestMapping(PAGE_LOGIN_ERROR)
    public String error(
            Model model,
            HttpServletRequest request,
            HttpServletResponse response) {
        addErrorMessage(request, "invalid-login");
        return VIEW_MAIN;
    }

    @RequestMapping(PAGE_403)
    public String accessDenied() {

        //check if user is login
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (!(auth instanceof AnonymousAuthenticationToken)) {
            UserDetails userDetail = (UserDetails) auth.getPrincipal();
            userDetail.getUsername();
        }

        return VIEW_403;
    }

}
