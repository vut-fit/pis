package cz.mjanys.web.login.pto;

/**
 * @author Martin Janys
 */
public class LoginPto {

    private String username_;
    private String password_;

    public String getUsername_() {
        return username_;
    }

    public void setUsername_(String username_) {
        this.username_ = username_;
    }

    public String getPassword_() {
        return password_;
    }

    public void setPassword_(String password_) {
        this.password_ = password_;
    }
}
