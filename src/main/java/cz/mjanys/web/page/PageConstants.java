package cz.mjanys.web.page;

/**
 * @author Martin Janys
 */
public class PageConstants {

    public static final String PAGE_MAPPING = "${page-root-mapping}";
    public static final String FRIENDLY_URL_MAPPING = "/{friendlyUrl}";

    public static final String PARAM_FRIENDLY_URL = "friendlyUrl";

    public static final String ATTR_PAGE = "page";
    public static final String ATTR_CHILD_PAGES = "childPages";

}
