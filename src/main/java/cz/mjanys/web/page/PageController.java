package cz.mjanys.web.page;

import cz.mjanys.iface.dto.Article;
import cz.mjanys.iface.exception.PageNotFound;
import cz.mjanys.iface.dto.Page;
import cz.mjanys.iface.service.PageService;
import cz.mjanys.web.AbstractController;
import cz.mjanys.web.layout.LayoutConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static cz.mjanys.web.page.PageConstants.*;

/**
 * @author Martin Janys
 */
@Controller
@RequestMapping(PAGE_MAPPING)
public class PageController extends AbstractController {

    @Autowired
    private PageService pageService;

    @Value("${page-layout-jsp-dir}")
    private String pageJsp;

    @Value("${article-layout-jsp-dir}")
    private String articleJsp;

    @ModelAttribute(LayoutConstants.ATTR_ARTICLE)
    protected Article article() {
        Article article = new Article();
        article.setArticleType(LayoutConstants.ARTICLE);
        return article;
    }

    @RequestMapping(FRIENDLY_URL_MAPPING)
    public String viewPage(
            @PathVariable(PARAM_FRIENDLY_URL) String friendlyUrl,
            @ModelAttribute(LayoutConstants.ATTR_ARTICLE) Article article,
            HttpServletRequest request,
            HttpServletResponse response) {

        Page page = pageService.findPageByFriendlyUrl(friendlyUrl);
        if (page == null) {
            throw new PageNotFound(friendlyUrl);
        }
        request.setAttribute(ATTR_PAGE, page);
        request.setAttribute(ATTR_CHILD_PAGES, pageService.childsOf(page));
        article.setPageId(page.getId());
        return pageJsp + "/" + page.getLayout();
    }

}
