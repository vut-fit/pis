package cz.mjanys.web.page;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author Martin Janys
 */
@Component
public class PageHelper {

    @Value("${page-root-mapping}")
    private String pageMapping;

    public String pageUri(String contextPath, String friendlyUrl) {
        return contextPath + pageMapping + "/" + friendlyUrl;
    }

    public String pageUri(String friendlyUrl) {
        return pageMapping + "/" + friendlyUrl;
    }
}
