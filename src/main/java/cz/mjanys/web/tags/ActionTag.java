package cz.mjanys.web.tags;

import cz.mjanys.iface.dto.Article;
import cz.mjanys.iface.dto.Page;
import cz.mjanys.web.http.HttpServletResponseStoringWrapper;
import cz.mjanys.web.layout.LayoutConstants;

import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;

/**
 * @author Martin Janys
 */
public class ActionTag extends SimpleTagSupport {

    private Article article;

    public void setArticle(Article article) {
        this.article = article;
    }

    public void doTag() throws JspException, IOException {
        PageContext pageContext = (PageContext) getJspContext();
        ServletRequest request = pageContext.getRequest();
        ServletResponse response = pageContext.getResponse();
        ServletContext servletContext = pageContext.getServletContext();

        RequestDispatcher requestDispatcher = servletContext.getRequestDispatcher(article.getAction());
        try {
            HttpServletResponseStoringWrapper storingWrapper = new HttpServletResponseStoringWrapper((HttpServletResponse) response);
            requestDispatcher.include(request, storingWrapper);
            byte[] bytes = storingWrapper.toByteArrayResponse();
            if (bytes != null)
                getJspContext().getOut().write(new String(bytes));
        }
        catch (ServletException e) {
            throw new RuntimeException(e);
        }
    }
}
