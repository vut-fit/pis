package cz.mjanys.web.user;

import cz.mjanys.web.Constants;

/**
 * @author Martin Janys
 */
public class UserConstants extends Constants {

    public static final String USER_MAPPING = "/admin/user";

    public static final String PAGE_USER_DETAIL = "/{id}";
    public static final String PAGE_USER_FORM_EDIT = "/edit/{id}";
    public static final String PAGE_USER_CREATE = "/create";

    public static final String ACTION_SAVE = "save";
    public static final String ACTION_DELETE = "delete";

    public static final String VIEW_MAIN = "user/view";
    public static final String VIEW_CREATE_FORM = "user/view_create";
    public static final String VIEW_EDIT_FORM = "user/view_edit";
    public static final String VIEW_DETAIL = "user/view_detail";

    public static final String ATTR_USERS = "users";
    public static final String ATTR_USER_PTO = "userPto";
    public static final String ATTR_USER_DTO = "user";
}
