package cz.mjanys.web.user;

import cz.mjanys.iface.dto.Role;
import cz.mjanys.iface.service.RoleService;
import cz.mjanys.web.AbstractController;
import cz.mjanys.web.user.converter.RolePropertyEditor;
import cz.mjanys.web.user.converter.UserPtoToDtoConverter;
import cz.mjanys.web.user.converter.UserDtoToPtoConverter;
import cz.mjanys.web.user.pto.UserPto;
import cz.mjanys.iface.dto.User;
import cz.mjanys.iface.service.UserService;
import cz.mjanys.web.user.validator.UserPtoValidator;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import java.io.IOException;
import java.security.Principal;
import java.util.List;
import java.util.Map;

import static cz.mjanys.web.user.UserConstants.*;

/**
 * @author Martin Janys
 */
@Controller
@RequestMapping(USER_MAPPING)
public class UserController extends AbstractController {

    private static final Logger LOG = Logger.getLogger(UserController.class);

    @Autowired
    private UserService userService;
    @Autowired
    private RoleService roleService;

    @Autowired
    private UserPtoToDtoConverter ptoToDto;
    @Autowired
    private UserDtoToPtoConverter dtoToPto;
    @Autowired
    private UserPtoValidator userPtoValidator;

    @ModelAttribute(ATTR_USER_PTO)
    public UserPto userPto() {
        return new UserPto(){{
            setRoles(dtoToPto.roleMap(roleService.findAllRoles(), false));
        }};
    }

    @ModelAttribute(ATTR_USERS)
    public List<User> users() {
        return userService.findAllUsers();
    }

    @InitBinder(ATTR_USER_PTO)
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Role.class, new RolePropertyEditor(roleService));
        binder.addValidators(userPtoValidator);
    }

    @RequestMapping
    public String defaultView(
            Model model,
            ServletRequest request,
            ServletResponse response) {

        return VIEW_MAIN;
    }

    @RequestMapping(PAGE_USER_DETAIL)
    public String detail(
            @PathVariable(PARAM_ID) long id,
            Model model,
            HttpServletRequest request,
            HttpServletResponse response) {

        model.addAttribute(ATTR_USER_DTO, userService.findUserById(id));

        return VIEW_DETAIL;
    }

    @RequestMapping(PAGE_USER_CREATE)
    public String create(
            Model model,
            ServletRequest request,
            ServletResponse response) {

        return VIEW_CREATE_FORM;
    }

    @RequestMapping(PAGE_USER_FORM_EDIT)
    public String editForm(
            @ModelAttribute UserPto pto,
            Model model,
            ServletRequest request,
            ServletResponse response) {

        User s = userService.findUserById(pto.getId());
        pto = dtoToPto.convert(s);
        fillUserRoles(pto);
        
        model.addAttribute(ATTR_USER_PTO, pto);

        return VIEW_EDIT_FORM;
    }

    private void fillUserRoles(UserPto pto) {
        Map<Role, Boolean> roles = pto.getRoles();
        for (Role role : roleService.findAllRoles()) {
            if (!roles.containsKey(role)) {
                roles.put(role, false);
            }
        }
    }

    @RequestMapping(value = ACTION_SAVE, method = RequestMethod.POST)
    public String createUser(
            @Valid @ModelAttribute(ATTR_USER_PTO) UserPto pto,
            BindingResult result,
            HttpServletRequest request,
            HttpServletResponse response,
            Model model,
            RedirectAttributes redirectAttrs) throws IOException {

        if (!result.hasErrors()) {
            User dto = ptoToDto.convert(pto);

            dto = userService.saveUser(dto);
            if (pto.getId() == null) {
                addSuccessMessage(request, "msg-user-created");
            }
            else {
                addSuccessMessage(request, "msg-user-saved");
            }

            response.sendRedirect(contextPath() + USER_MAPPING + "/" + dto.getId().toString());
            return null;
        }
        else {
            addErrorMessage(request, "msg-common-err-form-contains-errors");
            return VIEW_CREATE_FORM;
        }
    }

    @RequestMapping(value = ACTION_DELETE, method = RequestMethod.POST)
    public void deleteUser(
            @RequestParam(PARAM_ID) Long id,
            HttpServletRequest request,
            HttpServletResponse response,
            Model model,
            Principal principal) throws IOException {

        User dto = userService.findUserById(id);
        if (!principal.getName().equals(dto.getUsername())) {
            userService.deleteUserById(id);

            addSuccessMessage(request, "msg-user-deleted", dto.getUsername());
        }
        else {
            addErrorMessage(request, "msg-cant-deleted-current-user");
        }
        response.sendRedirect(contextPath() + USER_MAPPING);
    }


}
