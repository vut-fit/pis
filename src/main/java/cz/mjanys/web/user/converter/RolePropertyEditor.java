package cz.mjanys.web.user.converter;

import cz.mjanys.iface.service.RoleService;

import java.beans.PropertyEditorSupport;

/**
 * @author Martin Janys
 */
public class RolePropertyEditor extends PropertyEditorSupport {

    private final RoleService roleService;

    public RolePropertyEditor(RoleService roleService) {
        this.roleService = roleService;
    }

    @Override
    public void setAsText(String text) {
        setValue(roleService.findByRoleName(text));
    }

}
