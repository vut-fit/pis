package cz.mjanys.web.user.converter;

import com.google.common.collect.Maps;
import cz.mjanys.iface.dto.Role;
import cz.mjanys.web.user.pto.UserPto;
import cz.mjanys.iface.converter.generic.AbstractExtendedConverter;
import cz.mjanys.iface.dto.User;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.List;
import java.util.Map;

/**
 * @author Martin Janys
 */
@Component
public class UserDtoToPtoConverter extends AbstractExtendedConverter<User, UserPto> {

    @Override
    public UserPto newTarget() {
        return new UserPto();
    }

    @Override
    public UserPto convert(User source, UserPto target) {
        Assert.notNull(source);
        Assert.notNull(target);

        target.setId(source.getId());
        target.setUsername(source.getUsername());
        target.setPassword(source.getPassword());
        target.setEnable(source.isEnable());
        target.setRoles(roleMap(source.getRoles(), true));
        return target;
    }

    public Map<Role, Boolean> roleMap(Iterable<Role> allRoles, boolean flag) {
        Map<Role, Boolean> map = Maps.newHashMap();
        for (Role role : allRoles) {
            map.put(role, flag);
        }
        return map;
    }

}
