package cz.mjanys.web.user.converter;

import com.google.common.collect.Sets;
import cz.mjanys.iface.dto.Role;
import cz.mjanys.web.user.pto.UserPto;
import cz.mjanys.iface.converter.generic.AbstractExtendedConverter;
import cz.mjanys.iface.dto.User;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author Martin Janys
 */
@Component
public class UserPtoToDtoConverter extends AbstractExtendedConverter<UserPto, User> {

    @Override
    public User newTarget() {
        return new User();
    }

    @Override
    public User convert(UserPto source, User target) {
        Assert.notNull(source);
        Assert.notNull(target);

        target.setId(source.getId());
        target.setUsername(source.getUsername());
        target.setPassword(source.getPassword());
        target.setEnable(source.isEnable());
        target.setRoles(roles(source.getRoles()));
        return target;
    }

    private Set<Role> roles(Map<Role, Boolean> roles) {
        Set<Role> set = Sets.newHashSet();
        for (Map.Entry<Role, Boolean> entry : roles.entrySet()) {
            if (entry.getValue()) {
                set.add(entry.getKey());
            }
        }
        return set;
    }

}
