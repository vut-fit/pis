package cz.mjanys.web.user.pto;

import cz.mjanys.iface.DataLengthConstants;
import cz.mjanys.iface.dto.Role;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.Map;

/**
 * @author Martin Janys
 */
public class UserPto {

    private Long id;

    @NotNull
    @Size(min = 4, max = DataLengthConstants.STRING_SHORT)
    private String username;
    private String password;
    private boolean enable = true;
    private Map<Role, Boolean> roles;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public Map<Role, Boolean> getRoles() {
        return roles;
    }

    public void setRoles(Map<Role, Boolean> roles) {
        this.roles = roles;
    }
}
