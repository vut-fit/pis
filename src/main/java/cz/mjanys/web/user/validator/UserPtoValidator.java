package cz.mjanys.web.user.validator;

import cz.mjanys.iface.service.UserService;
import cz.mjanys.web.user.pto.UserPto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * @author Martin Janys
 */
@Component
public class UserPtoValidator implements Validator {

    @Autowired
    private UserService userService;

    @Override
    public boolean supports(Class<?> clazz) {
        return UserPto.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        UserPto userPto = (UserPto) target;

        if (userPto.getId() == null) {
            ValidationUtils.rejectIfEmpty(errors, "password", "not-empty");

            long count = userService.countByUsername(userPto.getUsername());
            if (count != 0) {
                errors.rejectValue("username", "msg-err-username-already-exists");
            }
        }
    }
}
