<%@include file="init.jspf"%>
<c:if test="${empty article.id}">
    <spring:message code="label-add-article" var="submitLabel"/>
    <c:set value="addArticleModalArticle" var="dialogId"/>
</c:if>
<c:if test="${not empty article.id}">
    <spring:message code="label-edit-article" var="submitLabel"/>
    <c:set value="editArticleModalArticle" var="dialogId"/>
</c:if>
<%@include file="/WEB-INF/jsp/fragments/articles/components/article_form.jspf"%>