<%@ page import="cz.mjanys.web.contactus.ContactUsConstants" %>
<%@include file="../init.jspf"%>

<div class="article padding-10">
    <form:form modelAttribute="<%=ContactUsConstants.ATTR_CONTACT_US_PTO%>">
        <f:inputrow path="email" labelCode="label-email" required="true"/>
        <f:textarearow path="message" labelCode="label-message" required="true"/>

        <bs:button type="submit" cssClass="btn-default">
            <spring:message code="label-submit"/>
        </bs:button>
    </form:form>
</div>
