<%@include file="../init.jspf"%>
<%--@elvariable id="parentPage" type="cz.mjanys.iface.dto.Page"--%>

<c:set var="curPage" value="${parentPage}"/>

<c:set var="parentPage" value="${parentPage.parent}" scope="request"/>
<c:if test="${not empty parentPage}">
    <jsp:include page="breadcrumb.jsp" flush="true"/>
</c:if>

<c:if test="${not curPage.root}">
    <li>
        <a href="${pageMapping}/${curPage.friendlyUrl}">
            <c:url value="${curPage.title}"/>
        </a>
    </li>
</c:if>
