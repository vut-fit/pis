<%@include file="../init.jspf"%>
<%--@elvariable id="page" type="cz.mjanys.iface.dto.Page"--%>
<%--@elvariable id="article" type="cz.mjanys.iface.dto.Article"--%>
<%--@elvariable id="articlesCount" type="java.lang.Integer"--%>
<%--@elvariable id="index" type="java.lang.Integer"--%>
<%--@elvariable id="first" type="java.lang.Boolean"--%>
<%--@elvariable id="last" type="java.lang.Boolean"--%>

<c:if test="${first}">
<div id="carousel" class="carousel slide article padding-10" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <c:forEach begin="0" end="${articlesCount}" varStatus="loop">
            <c:if test="${loop.first}">
                <li data-target="#carousel" data-slide-to="${loop.index}" class="active"></li>
            </c:if>
            <c:if test="${!loop.first}">
                <li data-target="#carousel" data-slide-to="${loop.index}"></li>
            </c:if>
        </c:forEach>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
</c:if>
        <div class="item <c:if test="${index == 0}">active</c:if>">
            <div class="carousel-caption">
                <h2>
                    <c:out value="${article.title}"/>
                </h2>
                <p>
                    ${article.content}
                </p>
            </div>
        </div>
<c:if test="${last}">
    </div>
    <!-- Controls -->
    <a class="left carousel-control" href="#carousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#carousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
<script type="text/javascript">
    $('.carousel').carousel();
</script>
</c:if>
