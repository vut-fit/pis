<%@include file="../init.jspf"%>
<%--@elvariable id="page" type="cz.mjanys.iface.dto.Page"--%>
<%--@elvariable id="article" type="cz.mjanys.iface.dto.Article"--%>
<%--@elvariable id="index" type="java.lang.Integer"--%>

<div class="article padding-10">
    <h2><c:out value="${article.title}"/></h2>
    <div class="article-content">
        ${article.content}
    </div>
    <%--<div class="article-author">--%>
        <%--<c:out value="${article.author}"/>--%>
    <%--</div>--%>
</div>