<%@include file="../init.jspf"%>
<%--@elvariable id="nodes" type="java.util.List<cz.mjanys.web.layout.pto.PageTreeNode>"--%>

<c:set var="currentNodes" value="${nodes}"/>
<ul>
    <c:forEach items="${currentNodes}" var="node">
        <li data-page-id="${node.id}">
            <c:url value="${pageMapping}/${node.friendlyUrl}" var="url"/>
            <c:if test="${empty node.nodes}">
                <span>
                    <i class="glyphicon glyphicon-leaf"></i>
                </span>
                <i class="glyphicon glyphicon-stop"></i>
                <a href="${url}">${node.text}</a>
                <i class="glyphicon glyphicon-eye-open <c:if test="${not node.visible}">glyphicon-eye-close</c:if>"></i>
                <%@include file="page_node_actions.jspf"%>
            </c:if>
            <c:if test="${not empty node.nodes}">
                <span>
                    <i class="glyphicon glyphicon-minus-sign"></i>
                </span>
                <i class="glyphicon glyphicon-stop"></i>
                <a href="${url}">${node.text}</a>
                <i class="glyphicon glyphicon-eye-open <c:if test="${not node.visible}">glyphicon-eye-close</c:if>"></i>
                <%@include file="page_node_actions.jspf"%>
                <c:set var="nodes" value="${node.nodes}" scope="request"/>
                <jsp:include page="/WEB-INF/jsp/layout/components/page_tree.jsp" flush="true"/>
            </c:if>
        </li>
    </c:forEach>
</ul>