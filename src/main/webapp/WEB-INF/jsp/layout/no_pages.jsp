<%@include file="init.jspf"%>

<t:html title="no-pages-title" contentClass="no-pages">

    <div style="line-height: 130px">
        <strong><span class="large-emoticon">:(</span><spring:message code="no-pages-exist"/></strong>
    </div>

</t:html>