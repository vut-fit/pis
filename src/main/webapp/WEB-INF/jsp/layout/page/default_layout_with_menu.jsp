<%@include file="../init.jspf"%>

<%--@elvariable id="page" type="cz.mjanys.iface.dto.Page"--%>
<%--@elvariable id="childPages" type="java.util.List<cz.mjanys.iface.dto.Page>"--%>

<t:html>
    <div class="row">
        <div class="col-md-2">
            <ul class="nav nav-pills nav-stacked">
                <c:forEach items="${childPages}" var="child">
                    <li role="presentation">
                        <a href="${pageMapping}/${child.friendlyUrl}">
                            <c:out value="${child.title}"/>
                        </a>
                    </li>
                </c:forEach>
            </ul>
        </div>
        <div class="col-md-10">
            <layout:content/>
        </div>
    </div>

</t:html>