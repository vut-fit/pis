<%@include file="../init.jspf"%>

<%--@elvariable id="page" type="cz.mjanys.iface.dto.Page"--%>

<t:html>
    <jsp:attribute name="headEnd">
        <style type="text/css">
            body {
                background-image: url('<c:url value="/css/green_bg.jpg"/>');
            }
            #page {
                padding: 8px 15px;
                margin-bottom: 20px;
                list-style: none;
                background-color: #f5f5f5;
                border-radius: 4px;
            }
            #footer {
                color: white;
                font-weight: bold;
            }
            .breadcrumb {
                padding-left: 0;
            }
        </style>
    </jsp:attribute>

    <jsp:body>
        <layout:content/>
    </jsp:body>
</t:html>