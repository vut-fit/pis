<%@include file="init.jspf"%>

<%--@elvariable id="pagePto" type="cz.mjanys.web.layout.pto.PagePto"--%>

<t:html>
    <h1>
        <spring:message code="label-add-page"/>
    </h1>
    <%@include file="components/page_form.jspf"%>
</t:html>