<%@include file="init.jspf"%>

<t:html>
    <jsp:attribute name="headEnd">
        <link href="<c:url value="/css/bootstrap-treeview.css"/>" rel="stylesheet">
        <script type="text/javascript" src="<c:url value="/js/tree.js"/>"></script>
    </jsp:attribute>
    <jsp:body>
        <h1>
            <spring:message code="label-manage-pages"/>
        </h1>

        <div>
            <%@include file="components/root_page_tree.jspf"%>
        </div>
    </jsp:body>
</t:html>