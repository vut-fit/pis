<%--@elvariable id="items" type="java.util.List<cz.mjanys.iface.dto.User>"--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@include file="init.jspf" %>

<%@page import="static cz.mjanys.web.user.UserConstants.*" %>

<t:html contentClass="user-app">
    <div id="userView">
        <div class="panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading">All users.</div>
            <%--<div class="panel-body"></div>--%>

            <!-- Table -->
            <%@include file="components/table.jspf"%>

            <div class="panel-footer">
                <c:url value="/admin/user/create" var="createUrl"/>
                <h:link href="${createUrl}">
                    <spring:message code="label-create-user" var="createLabel"/>
                    <bs:button text="${createLabel}" cssClass="btn-default"/>
                </h:link>
            </div>
        </div>
    </div>
</t:html>
