<%--@elvariable id="user" type="cz.mjanys.iface.dto.User"--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@include file="init.jspf" %>

<c:set var="col1" value="col-md-2 bold"/>
<c:set var="col2" value="col-md-10"/>

<t:html>
    <h2><spring:message code="label-user-detail-page"/></h2>
    <div class="list-group">
        <div class="row list-group-item">
            <div class="${col1}">
                <spring:message code="label-id"/>:
            </div>
            <div class="${col2}">
                <c:out value="${user.id}"/>
            </div>
        </div>
        <div class="row list-group-item">
            <div class="${col1}">
                <spring:message code="label-username"/>:
            </div>
            <div class="${col2}">
                <c:out value="${user.username}"/>
            </div>
        </div>
        <div class="row list-group-item">
            <div class="${col1}">
                <spring:message code="label-enable"/>:
            </div>
            <div class="${col2}">
                <c:if test="${user.enable}">
                    <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                </c:if>
                <c:if test="${not user.enable}">
                    <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                </c:if>
            </div>
        </div>
        <div class="row list-group-item">
            <div class="${col1}">
                <spring:message code="label-roles"/>:
            </div>
            <div class="${col2}">
                <%--@elvariable id="role" type="cz.mjanys.iface.dto.Role"--%>
                <c:forEach items="${user.roles}" var="role" varStatus="loop">
                    ${role.role}${!loop.last ? ', ' : ' '}
                </c:forEach>
            </div>
        </div>
        <div class="row list-group-item">
            <div class="col-md-4">
                <c:url value="/admin/user" var="userUrl"/>
                <h:link href="${userUrl}">
                    <spring:message code="label-user-page" var="userPageLabel"/>
                    <bs:button text="${userPageLabel}" cssClass="btn-default"/>
                </h:link>
            </div>
            <div class="col-md-4">
                <c:url value="/admin/user/edit/${user.id}" var="updateUrl"/>
                <h:link href="${updateUrl}">
                    <spring:message code="label-update-user" var="updateLabel"/>
                    <bs:button text="${updateLabel}" cssClass="btn-default"/>
                </h:link>
            </div>
            <a class="col-md-4">
                <c:url value="/admin/user/delete" var="deleteUrl"/>
                <form:form id="deleteForm" action="${deleteUrl}" method="post">
                    <input type="hidden" name="id" value="${user.id}"/>
                </form:form>
                <spring:message code="msg-confirm-delete" var="really"/>
                <h:link href="javascript:$('#deleteForm').submit()" onclick="return confirm('${really}')">
                    <spring:message code="label-delete-user" var="deleteLabel"/>
                    <bs:button text="${deleteLabel}" cssClass="btn-danger"/>
                </h:link>
            </a>
        </div>
    </div>
</t:html>