<%--@elvariable id="items" type="java.util.List<cz.mjanys.iface.dto.User>"--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@include file="init.jspf" %>

<t:html contentClass="user-app">
    <h2>
        <spring:message code="label-update-user"/>
    </h2>
    <div class="panel">
        <%@include file="components/form.jspf"%>
    </div>
</t:html>