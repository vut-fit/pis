<%@tag description="Input" pageEncoding="UTF-8"%>

<%--@elvariable id="path" type="java.lang.String"--%>
<%--@elvariable id="id" type="java.lang.String"--%>
<%--@elvariable id="hasErrors" type="java.lang.Boolean"--%>

<%@include file="../init.jspf"%>

<form:checkbox path="${path}" id="${id}" cssClass="checkbox"/>
<c:if test="${hasErrors}">
    <span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
    <span id="inputError2Status" class="sr-only">(error)</span>
</c:if>