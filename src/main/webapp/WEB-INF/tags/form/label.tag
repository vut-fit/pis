<%@tag description="Label" pageEncoding="UTF-8"%>

<%@include file="../init.jspf"%>

<%@attribute name="code"%>

<c:if test="${not empty code}">
    <spring:message code="${code}" var="title"/>
    <form:label path="${path}" title="${title}" cssClass="large control-label">
        ${title}
        <jsp:doBody/>
    </form:label>:
</c:if>