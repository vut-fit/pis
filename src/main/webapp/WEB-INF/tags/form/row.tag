<%@tag description="Row" pageEncoding="UTF-8"%>

<%@include file="../init.jspf"%>

<%@attribute name="path" required="true" %>
<%@attribute name="required" %>

<c:set var="path" value="${path}" scope="request"/>

<c:set var="errors">
    <form:errors path="${path}"/>
</c:set>
<c:set var="hasErrors" value="${not empty errors}" scope="request"/>

<c:if test="${hasErrors}">
    <c:set var="errorClass" value="has-error has-feedback"/>
</c:if>

<c:if test="${required}">
    <c:set var="css" value="required"/>
</c:if>

<div class="form-group ${css} ${errorClass}">
    <jsp:doBody/>
</div>
