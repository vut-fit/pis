<%@tag description="Input" pageEncoding="UTF-8"%>

<%--@elvariable id="path" type="java.lang.String"--%>
<%--@elvariable id="id" type="java.lang.String"--%>
<%--@elvariable id="hasErrors" type="java.lang.Boolean"--%>

<%@attribute name="items" type="java.util.List" %>
<%@attribute name="itemLabel" type="java.lang.String" %>
<%@attribute name="itemValue" type="java.lang.String" %>

<%@include file="../init.jspf"%>

<form:select path="${path}" id="${id}" cssClass="form-control">
    <form:options items="${items}" itemLabel="${itemLabel}" itemValue="${itemValue}"/>
</form:select>
<c:if test="${hasErrors}">
    <span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
    <span id="inputError2Status" class="sr-only">(error)</span>
</c:if>