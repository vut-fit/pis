<%@tag description="Input Row" pageEncoding="UTF-8"%>

<%@include file="../init.jspf"%>

<%@attribute name="path" type="java.lang.String" required="true" %>
<%@attribute name="labelCode" type="java.lang.String" required="true" %>
<%@attribute name="required" type="java.lang.Boolean" %>
<%@attribute name="items" type="java.util.List" %>
<%@attribute name="itemLabel" type="java.lang.String" %>
<%@attribute name="itemValue" type="java.lang.String" %>

<c:set var="path" value="${path}" scope="request"/>
<c:set var="labelCode" value="${labelCode}" scope="request"/>

<f:row path="${path}" required="${required}">
    <f:label code="${labelCode}"/>
    <f:select items="${items}" itemLabel="${itemLabel}" itemValue="${itemValue}"/>
    <f:errors/>
</f:row>
