<%@tag description="Input Row" pageEncoding="UTF-8"%>

<%@include file="../init.jspf"%>

<%@attribute name="id" type="java.lang.String" required="true" %>
<%@attribute name="path" type="java.lang.String" required="true" %>
<%@attribute name="labelCode" type="java.lang.String" required="true" %>
<%@attribute name="required" type="java.lang.Boolean" %>
<%@attribute name="dialogId" type="java.lang.String" %>

<c:set var="id" value="${id}" scope="request"/>
<c:set var="path" value="${path}" scope="request"/>
<c:set var="summerNoteId" value="${id}_summernote"/>
<c:set var="labelCode" value="${labelCode}" scope="request"/>

<f:row path="${path}" required="${required}">
    <f:label code="${labelCode}"/>
    <div style="display: none">
        <f:input/>
    </div>
    <div id="${summerNoteId}"></div>
    <f:errors/>
</f:row>

<script type="text/javascript">
    if ($.summernote) {
        var write = function(e) {
            $("#${id}").val($("#${summerNoteId}").code());
        };
        $('#' + '${summerNoteId}').summernote({
            onkeyup: write,
            onblur: write,
            oninit: function () {
                $("#${summerNoteId}").code($("#${id}").val())
            }
        });
    }
</script>
