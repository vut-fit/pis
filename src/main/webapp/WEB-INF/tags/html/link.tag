<%@tag description="Link" pageEncoding="UTF-8"%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@attribute name="href" type="java.lang.String" required="true" %>
<%@attribute name="onclick" type="java.lang.String" %>
<%@attribute name="cssClass" type="java.lang.String" %>

<a href="${href}" <c:if test="${not empty onclick}">onclick="${onclick}"</c:if> <c:if test="${not empty cssClass}">class="${cssClass}"</c:if>>
    <jsp:doBody/>
</a>