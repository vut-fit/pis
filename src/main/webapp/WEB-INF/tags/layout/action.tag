<%@include file="../init.jspf"%>
<%@taglib prefix="action" uri="/WEB-INF/tags/action.tld" %>

<%@attribute name="article" type="cz.mjanys.iface.dto.Article" %>

<action:action article="${article}"/>