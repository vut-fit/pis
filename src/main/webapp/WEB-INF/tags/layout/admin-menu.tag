<%@include file="../init.jspf"%>
<%--TODO nemusi byt tag--%>
<%--@elvariable id="rootPage" type="cz.mjanys.iface.dto.RootPage"--%>
<sec:authorize ifAllGranted="EDITOR_ROLE">
    <div id="admin-menu">
        <c:set var="liClass" value="list-group-item"/>
        <div class="list-group">
            <%@include file="/WEB-INF/jsp/fragments/page/page_admin_menu_crud_items.jspf"%>
        </div>
        <div class="list-group">
            <%@include file="/WEB-INF/jsp/fragments/page/page_admin_menu_manage_item.jspf"%>
        </div>
    </div>
</sec:authorize>