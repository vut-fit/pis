<%@include file="../init.jspf"%>

<%@attribute name="articles" type="java.util.List<cz.mjanys.iface.dto.Article>" %>

<c:if test="${empty viewMode}">
    <c:set var="viewMode" value="false"/>
</c:if>

<%@include file="/WEB-INF/jsp/fragments/articles/articles.jspf"%>
