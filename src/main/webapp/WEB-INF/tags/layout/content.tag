<%@include file="../init.jspf"%>

<%--@elvariable id="page" type="cz.mjanys.iface.dto.Page"--%>

<div class="page-content">
    <layout:articles articles="${page.articles}"/>
</div>
