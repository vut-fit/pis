<%@include file="init.jspf"%>

<%@attribute name="root" type="cz.mjanys.iface.dto.HirearchialPage" %>

<%--@elvariable id="root" type="cz.mjanys.iface.dto.Page"--%>

<%--@elvariable id="page" type="cz.mjanys.iface.dto.Page"--%>
<c:forEach items="${root.childs}" var="page">
    <c:if test="${page.visible}">
        <c:if test="${not page.hasVisibleChildren()}">
            <li>
                <a href="<c:url value="${pageMapping}/${page.friendlyUrl}"/>">
                    ${page.title}
                </a>
            </li>
        </c:if>
        <c:if test="${page.hasVisibleChildren()}">
            <li class="dropdown <c:if test="${not root.root}">dropdown-submenu</c:if>">
                <a href="<c:url value="${pageMapping}/${page.friendlyUrl}"/>">
                    <c:out value="${page.title}"/>
                    <c:if test="${root.root}"><b class="glyphicon glyphicon-chevron-down menu-caret"></b></c:if>
                    <c:if test="${not root.root}"><b class="glyphicon glyphicon-chevron-right menu-caret"></b></c:if>
                </a>
                <ul class="dropdown-menu">
                    <layout:page-menu root="${page}"/>
                </ul>
            </li>
        </c:if>
    </c:if>
</c:forEach>
