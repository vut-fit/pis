<%@tag description="Head Template" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<%@attribute name="headBegin" fragment="true" %>
<%@attribute name="headEnd" fragment="true" %>
<%@attribute name="title"%>

<c:if test="${empty title}">
    <spring:message code="main-page-title" var="title"/>
</c:if>
<c:if test="${not empty title}">
    <spring:message code="${title}" var="title"/>
</c:if>

<head>
    <jsp:invoke fragment="headBegin"/>

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>${title}</title>

    <sec:authorize ifAnyGranted="ADMIN_ROLE, EDITOR_ROLE">
        <link href="<c:url value="/css/tree.css"/>" rel="stylesheet">
        <link href="<c:url value="/css/summernote.css"/>" rel="stylesheet">
        <link href="<c:url value="/css/summernote-bs3.css"/>" rel="stylesheet">
        <link href="<c:url value="/css/font-awesome.min.css"/>" rel="stylesheet">
        <link href="<c:url value="/css/ajaxmask.css"/>" rel="stylesheet">
    </sec:authorize>

    <%--<link rel="shortcut icon" href="<c:url value="/img/favicon.png"/>">--%>
    <link href="<c:url value="/css/bootstrap.css"/>" rel="stylesheet">
    <link href="<c:url value="/css/style.css"/>" rel="stylesheet">

    <script type="text/javascript" src="<c:url value="/js/jquery.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/js/bootstrap.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/js/script.js"/>"></script>

    <sec:authorize ifAnyGranted="ADMIN_ROLE, EDITOR_ROLE">
        <script type="text/javascript" src="<c:url value="/js/summernote.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/js/jquery-ui.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/js/ajaxmask.js"/>"></script>
    </sec:authorize>

    <jsp:invoke fragment="headEnd"/>
</head>