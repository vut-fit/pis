<%@tag description="Header Template" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--@elvariable id="page" type="cz.mjanys.iface.dto.Page"--%>
<header id="pageheader" class="container">
    <c:if test="${not empty page}">
        <ol class="breadcrumb">
            <c:set var="parentPage" value="${page.parent}" scope="request"/>
            <jsp:include page="/WEB-INF/jsp/fragments/breadcrumb.jsp" flush="true"/>
            <li class="active">
                ${page.title}
            </li>
        </ol>
    </c:if>
</header>
