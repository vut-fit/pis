<%@tag description="Menu Template" pageEncoding="UTF-8"%>

<%@include file="init.jspf"%>

<nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <c:url value="/" var="url"/>
            <a class="navbar-brand" href="${url}">
                <spring:message code="main-page-title"/>
            </a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul id="page-menu" class="nav navbar-nav">
                <layout:page-menu root="${rootPage}"/>
            </ul>
            <%--<form class="navbar-form navbar-left" role="search">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Search">
                </div>
                <button type="submit" class="btn btn-default">
                    Submit
                </button>
            </form>--%>
            <%@include file="/WEB-INF/jsp/fragments/admin_menu.jspf"%>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>