function reloadArticleEdit(id, url) {
    var selector = '#' + id + "_ajax";
    var $elem = $(selector);
    $elem.hide();
    $elem.load(url, function () {
        $elem.fadeIn();
    });
}

// DROPDOWN - http://www.bootply.com/nZaxpxfiXz
(function($){
    $(document).ready(function(){
        $('.menu-caret').on('click', function(event) {
            event.preventDefault();
            event.stopPropagation();
            $(this).closest("li").siblings().removeClass('open');
            $(this).closest("li").toggleClass('open');
        });
        $('body').on('click', function () {
            $("#page-menu").find("li").removeClass('open');
        })
    });
})(jQuery);